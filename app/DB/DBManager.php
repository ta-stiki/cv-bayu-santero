<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/17/2017
 * Time: 9:20 PM
 */

namespace app\DB;


/**
 * Class DBManager
 * @package app\DB
 */
class DBManager
{
    /**
     * @var \mysqli
     */
    private $db;
    /**
     * @var array
     */
    private $config;

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }
    /**
     * DBManager constructor.
     * @param $config
     */
    public function __construct($config)
    {
        try{
            $this->config = $config['db'];
        } catch (\Exception $exception){
            echo 'Terjadi kesalahan konfigurasi. ' . $exception->getMessage();
        }
    }

    /**
     *
     */
    public function openConnection()
    {
        try{
            $configDB = $this->config;
            $this->db = new  \mysqli($configDB['host'],$configDB['username'],$configDB['password'],$configDB['database']);
        } catch (\mysqli_sql_exception $ex){
            echo 'Terjadi kesalahan koneksi database. ' . $ex->getMessage();
        } catch (\Exception $exception){
            echo 'Terjadi kesalahan konfigurasi. ' . $exception->getMessage();
        }
    }

    /**
     *
     */
    public function closeConnection()
    {
        if (isset($this->db)){
            $this->db->close();
            unset($this->db);
        }
    }

    /**
     * @param $sql
     * @param null $report_error
     * @param string $error_msg
     * @return bool|\mysqli_result
     */
    public function query($sql, $report_error = NULL, &$error_msg = '') {
        $this::openConnection();
        $pdo_stmt = $this->db->query($sql);
        return $pdo_stmt;
    }


    /**
     * @param bool| \mysqli_result $rs
     * @param string $type
     * @return int
     */
    public function result($rs, $type = 'assoc') {
        switch($type) {
            case 'assoc':
                $out_value = $rs->fetch_assoc();
                break;
            case 'array':
                $out_value = $rs->fetch_array();
                break;
            case 'row':
                $out_value = $rs->fetch_row();
                break;
            // TODO: Support PDO::FETCH_COLUMN , PDO::FETCH_CLASS , PDO::FETCH_INTO, PDO::FETCH_BOTH , PDO::FETCH_BOUND, PDO::FETCH_LAZY, PDO::FETCH_NUM, PDO::FETCH_OBJ
            case 'object':
                $out_value = $rs->fetch_object();
                break;
            case 'field':
                $out_value = $rs->fetch_field();
                break;
            case 'num_rows_affected':
                $out_value = (int)$rs->fetch_row();
                break;
            case 'num_fields':
                $out_value = (int)$rs->field_count;
                break;
            case 'num_rows':
                $out_value = count($rs->fetch_all());
                break;
            default:
                $out_value = $rs->fetch_all();
                break;
        }
        return $out_value;
    }

    public function insertID()
    {
        return $this->db->insert_id;
    }


}