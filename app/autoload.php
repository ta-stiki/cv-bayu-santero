<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/17/2017
 * Time: 9:13 PM
 */

spl_autoload_register(function ($class){
    $prefix = 'app\\';
    $length = strlen($prefix);
    $baseDir = __DIR__ . '/';
    if(strncmp($prefix,$class,$length) !==0){
        return;
    }
    $relative_class = substr($class, $length);
    $file = $baseDir .str_replace('\\','/',$relative_class) . '.php';
    if(file_exists($file)){
        require $file;
    }
});