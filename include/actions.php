<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:10 AM
 */

include_once dirname(__DIR__). '/include/koneksi.php';
include_once dirname(__DIR__). '/include/fungsi.php';

$a = (isset($_GET['a']))? escape($_GET['a']):null;
$q = (isset($_GET['q']))? escape($_GET['q']):null;
$id = (isset($_GET['id']))? escape($_GET['id']):null;
$id2 = (isset($_GET['id2']))? escape($_GET['id2']):null;

switch ($a){
    case 'tamu_auto':
        $dataTamu = getNamaDataTamu($q);
        $result = array();
        if($dataTamu){
            while ($row = $dataTamu->fetch_object()){
                $result[] = [
                    'id' => $row->nama_tamu,
                    'label' => $row->nama_tamu,
                    'value' => $row->nama_tamu,
                ];

            }
            echo json_encode($result);
            die();
        }
        break;
     case 'tarif_proses':
        $dataProse = showMasterProsesVisaByID($id);
        if($dataProse){
            echo $dataProse->harga;
            die();
        }else{
            echo 0;
        }
        break;

    case 'list_tamu':
        $agen = showDataTamu();
        echo "<option value=\"\">--Pilih Tamu--</option>";
        while ($rowAgen = $agen->fetch_object()){
            echo "<option value=\"{$rowAgen->id_tamu}\" $select>{$rowAgen->nama_tamu}</option>";
        }
        break;

    case 'list_tamu_tunggakan':
        $tamu = getPiutang();
        echo "<option value=\"\">--Pilih Tamu--</option>";
        while ($rowAgen = $tamu->fetch_object()){
            echo "<option value=\"{$rowAgen->id_tamu}\" $select>{$rowAgen->nama_tamu} [{$rowAgen->sisa_pembayaran}]</option>";
        }
        break;

    case 'list_prose':
        $proses = showMasterProsesVisa();
        echo "<option value=\"\">--Pilih Proses--</option>";
        while ($rowAgen = $proses->fetch_object()){
            echo "<option value=\"{$rowAgen->id}\" $select>{$rowAgen->proses}</option>";
        }
        break;
    case 'list_proses_tunggakan':
        $tamu = getPiutangByIDTamu($id);
        echo "<option value=\"\">--Pilih Proses--</option>";
        if($tamu){
            while ($rowAgen = $tamu->fetch_object()){
                echo "<option value=\"{$rowAgen->id_proses}\" $select>{$rowAgen->proses}</option>";
            }
        }

        break;

    case 'jumlah_tunggakan':
        $tamu = getPiutangByIDTamuAndProses($id,$id2);
        echo $tamu;
        break;

    case 'grafik':
        $grafik = showLabaRugiGrafik($q);
        $pendapatan = 0;
        $biaya = 0;
        $result['labels']= ['Pemasukan', 'Pengeluaran', 'Keuntungan'];

        if($grafik){
            while($item = $grafik->fetch_object()){
                $pendapatan += $item->D;
                $biaya += $item->F;
            }
        }

        $result['datasets'][]= [
            'label' => 'Total Transaksi',
            'backgroundColor' => [
                '#3dd300',
                '#f49200',
                '#fff800',
            ],
            'data' => [$pendapatan, $biaya, $pendapatan - $biaya],
        ];

        echo json_encode($result);
        break;



}