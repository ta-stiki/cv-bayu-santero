<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/25/2017
 * Time: 9:17 AM
 */

include_once __DIR__ . '/fungsi.php';

$host="localhost";
$user="root";
$pass="";
$db="acc_sriwijayanti";

$mysqli = new mysqli($host,$user,$pass,$db);


/**
 * Proses login kedalam sistem
 * @param string $username
 * @param string $password
 * @return bool|string
 */
function doLogin($username, $password){
    global $mysqli;

    if($username && $password){
        $password = escape($password);
        $queryString = "SELECT * FROM tb_user WHERE username = '{$username}' AND password='{$password}' LIMIT 1";
        $query = $mysqli->query($queryString);

        if($query){
            if($query->num_rows){
                start_session();
                $data = $query->fetch_object();
                $_SESSION['accApp'] = [
                    'akses' => $data->akses,
                    'user_id' => $data->id_user,
                    'nama_user' => $data->nama_user,
                ];
                session_write_close();
                return true;
            }
        }
    }else{
        return "Maaf username atau password tidak sesuai";
    }
    return false;
}

/**
 * Proses logout sistem
 * @param $path
 */
function doLogout($path){
    session_destroy();
    header("Location:{$path}");
    exit();
}

/**
 * @param $query
 * @return mysqli_result
 */
function query($query){
    global $mysqli;
    try{
        $query = $mysqli->query($query);
        if($query){
            if($query->num_rows){
                return $query;
            }
        }
    }catch (mysqli_sql_exception $e){
        print_r($e->getMessage());
    }
}

/**
 * @param $query
 * @return mixed
 */
function insert($query){
    global $mysqli;
    query($query);
    return $mysqli->insert_id;
}

/**
 * @return mixed
 */
function get_insert_id(){
    global $mysqli;
    return $mysqli->insert_id;
}

/**
 * Escape String
 * @param $string
 * @return string
 */
function escape($string){
    global $mysqli;
    return $mysqli->real_escape_string($string);
}


/*=============================DAFTAR QUERY==========================*/
/*===========DATA Country=============*/
/**
 * @return mysqli_result
 */
function showListCountry(){
    $strQuery = "SELECT * FROM apps_countries";
    $query = query($strQuery);
    return $query;
}
/*===========DATA Country=============*/
/*===========DATA USER=============*/
/**
 * Fungsi untuk menampilkan data user
 * @param string $q
 * @return mysqli_result
 */
function showUser($q = null){
    $strQuery = "SELECT * FROM tb_user";
    if($q){
        $strQuery .= " WHERE tb_user.nama_user LIKE '%{$q}%' OR tb_user.username = '{$q}' OR tb_user.akses = '{$q}' ";
    }
    $query = query($strQuery);
    return $query;
}

/**
 * Fungsi untuk pencarian user berdasarkan id user
 * @param $id
 * @return bool|object|stdClass
 */
function showDataUserByID($id){
    $strQuery = "SELECT * FROM tb_user";
    if($id){
        $strQuery .= " WHERE tb_user.id_user = '$id'";
    }
    /**
     * @var $query mysqli_result
     **/
    $query = query($strQuery);
    if($query->num_rows){
        return $query->fetch_object();
    }else{
        return false;
    }
}

/*===========DATA USER=============*/
/*===========DATA TAMU=============*/
/**
 * Fungsi query data tamu
 * @param string $q Parameter pencarian data tamu berdasarkan nama atau kewarganegaraan
 * @return mysqli_result
 */
function showDataTamu($q = null){
    $strQuery = "SELECT tamu.*, agen.nama_agen, master_proses_visa.proses as proses_visa FROM tamu 
      INNER JOIN agen ON tamu.id_agen = agen.id_agen
      INNER JOIN master_proses_visa ON tamu.proses = master_proses_visa.id
    ";
    if($q){
        $strQuery .= " WHERE tamu.nama_tamu LIKE '%{$q}%' OR tamu.kewarganegaraan like '%{$q}%'";
    }
    $query = query($strQuery);
    return $query;
}

/**
 * @param null $q
 * @return mysqli_result
 */
function getNamaDataTamu($q = null){
    $strQuery = "SELECT nama_tamu FROM tamu";
    if($q){
        $strQuery .= " WHERE tamu.nama_tamu LIKE '%{$q}%'";
    }
    $query = query($strQuery);
    return $query;
}

/**
 * Fungsi Pencarin tamu berdasarkan ID
 * @param string $id IdTamu
 * @return bool|object|stdClass
 */
function showDataTamuByID($id){
    $strQuery = "SELECT tamu.*, agen.nama_agen FROM tamu INNER JOIN agen ON tamu.id_agen = agen.id_agen";
    if($id){
        $strQuery .= " WHERE tamu.id_tamu = '$id'";
    }
    /**
     * @var $query mysqli_result
    **/
    $query = query($strQuery);
    if($query->num_rows){
        return $query->fetch_object();
    }else{
        return false;
    }
}
/*===========DATA TAMU=============*/
/*===========DATA AGEN=============*/
/**
 * Fungsi query data agen
 * @param string $q Parameter Pencarian data agen berdasarkan nama agen atau alamat agen atau no telp
 * @return mysqli_result
 */
function showDataAgen($q = null){
    $strQuery = "SELECT * FROM agen";
    if($q){
        $strQuery .= " WHERE nama_agen LIKE '%{$q}%' OR alamat_agen like '%{$q}%' OR no_telp like '%{$q}%'";
    }
    $query = query($strQuery);
    return $query;
}

/**
 * @param $id
 * @return bool|object|stdClass
 */
function showDataAgenByID($id){
    $strQuery = "SELECT * FROM agen ";
    if($id){
        $strQuery .= " WHERE id_agen = '$id'";
    }
    /**
     * @var $query mysqli_result
     **/
    $query = query($strQuery);
    if($query->num_rows){
        return $query->fetch_object();
    }else{
        return false;
    }
}
/*===========DATA AGEN=============*/
/*===========DATA KEWARGANEGARAAN=============*/
/**
 * Fungsi query data agen
 * @param string $q Parameter Pencarian data KEWARGANEGARAAN berdasarkan nama agen atau alamat agen atau no telp
 * @return mysqli_result
 */
function showDataKewarganegaraan($q = null){
    $strQuery = "SELECT * FROM apps_countries";
    if($q){
        $strQuery .= " WHERE country_code LIKE '%{$q}%' OR country_name like '%{$q}%'";
    }
    $query = query($strQuery);
    return $query;
}

/**
 * @param $id
 * @return bool|object|stdClass
 */
function showDataKewarganegaraanByID($id){
    $strQuery = "SELECT * FROM apps_countries ";
    if($id){
        $strQuery .= " WHERE id = '$id'";
    }
    /**
     * @var $query mysqli_result
     **/
    $query = query($strQuery);
    if($query->num_rows){
        return $query->fetch_object();
    }else{
        return false;
    }
}
/*===========DATA KEWARGANEGARAAN=============*/
/*===========DATA Akun Klasifikasi=============*/
function showDataAkunKlasifikasi($q = null){
    $strQuery = "select * from akun_klasifikasi";
    if($q){
        $strQuery .= " WHERE klasifikasi LIKE '%{$q}%' OR deskripsi like '%{$q}%' OR normal like '%{$q}%' OR inisial like '%{$q}%'";
    }
    $strQuery .= " order by klasifikasi asc";
    $query = query($strQuery);
    return $query;
}
/*===========DATA Akun Klasifikasi=============*/


/*===========DATA AKUN=============*/
/**
 * @param null $q
 * @return mysqli_result
 */
function showDataAkun($q = null){
    $strQuery = "select * from akun ";
    if($q){
        $strQuery .= " WHERE kode_rekening LIKE '%{$q}%' OR nama_rekening like '%{$q}%' OR posisi like '%{$q}%' OR normal like '%{$q}%'";
    }
    $strQuery .= " order by kode_rekening asc";
    $query = query($strQuery);
    return $query;
}

/**
 * @param $id
 * @return bool|object|stdClass
 */
function showDataAkunByID($id){
    $strQuery = "SELECT * FROM akun ";
    if($id){
        $strQuery .= " WHERE kode_rekening = '$id'";
    }
    /**
     * @var $query mysqli_result
     **/
    $query = query($strQuery);
    if($query->num_rows){
        return $query->fetch_object();
    }else{
        return false;
    }
}

function listOptionDataakun($select = null){
    $akun = showDataAkun();
    if($akun){
        while($a = $akun->fetch_object()){
            $selected = ($select == $a->kode_rekening)? 'selected':null;
            echo "<option $selected value='{$a->kode_rekening}'>{$a->kode_rekening}-{$a->nama_rekening}</option>";
        }
    }
}
/*===========DATA AKUN=============*/
/*===========LABA RUGI=============*/
/**
 * @param null $q
 * @return mysqli_result
 */
function showLabaRugi($q = null, $dari = null, $sampai = null){
    $dari = ($dari)? dmyToYmd($dari,'/'):null;
    $sampai = ($sampai)? dmyToYmd($sampai,'/'):null;

    $dateFilter = 'WHERE ';
    if($dari && $sampai){
        $dateFilter .= "j.tanggal BETWEEN '{$dari}' AND '{$sampai}'";
    }elseif(!$dari && $sampai){
        $dateFilter .= "j.tanggal <= '{$sampai}'";
    }elseif ($dari && !$sampai){
        $dateFilter .= "j.tanggal >= '{$dari}'";
    }else{
        $dateFilter = '';
    }


    $strQuery = "SELECT a.*, jurnal.debit, jurnal.credit FROM akun as a
LEFT JOIN
(SELECT
jd.kode_akun,
Sum(jd.debit) AS debit,
Sum(jd.credit) AS credit,
j.tanggal
FROM
jurnal_detail AS jd
INNER JOIN jurnal AS j ON jd.id_jurnal = j.id
{$dateFilter}
GROUP BY
jd.kode_akun ) as jurnal ON a.kode_rekening = jurnal.kode_akun ";

    if($q){
        $strQuery .= " WHERE a.kode_rekening LIKE '%{$q}%' OR a.nama_rekening like '%{$q}%' AND a.klasifikasi IN ('D','F')";
    }
    $strQuery .= " order by a.kode_rekening asc";
//    echo $strQuery;
    $query = query($strQuery);
    return $query;
}

function showLabaRugiGrafik($year){
    $strQuery = "SELECT a.*, 
SUM(CASE WHEN a.klasifikasi = 'D' THEN jurnal.credit END) AS D,
SUM(CASE WHEN a.klasifikasi = 'F' THEN jurnal.debit END) AS F

FROM akun as a LEFT JOIN (SELECT jd.kode_akun, Sum(jd.debit) AS debit, Sum(jd.credit) AS credit, j.tanggal FROM jurnal_detail AS jd INNER JOIN jurnal AS j ON jd.id_jurnal = j.id 
WHERE DATE_FORMAT(j.tanggal, '%m-%Y') = '{$year}' 
GROUP BY jd.kode_akun ) as jurnal ON a.kode_rekening = jurnal.kode_akun 
where a.klasifikasi IN ('D','F')
GROUP BY a.klasifikasi
order by a.kode_rekening asc";
    $query = query($strQuery);
    return $query;
}
/*===========LABA RUGI=============*/
/*===========MASTER PROSES VISA=============*/
/**
 * @param null $q
 * @return mysqli_result
 */
function showMasterProsesVisa($q = null){
    $strQuery = "SELECT * FROM master_proses_visa";
    if($q){
        $strQuery .= " WHERE proses LIKE '%{$q}%'";
    }
    $query = query($strQuery);
    return $query;
}

/**
 * @param $id
 * @return bool|object|stdClass
 */
function showMasterProsesVisaByID($id){
    $strQuery = "SELECT * FROM master_proses_visa";
    if($id){
        $strQuery .= " WHERE id = '$id'";
    }
    /**
     * @var $query mysqli_result
     **/
    $query = query($strQuery);
    if($query->num_rows){
        return $query->fetch_object();
    }else{
        return false;
    }
}

/**
 * @param $id
 * @return string
 */
function getNameProsesVisa($id){
    $data = showMasterProsesVisaByID($id);
    if($data){
        return $data->proses;
    }else{
        return '';
    }
}
/*===========MASTER PROSES VISA=============*/
/*===========TRANSAKSI=============*/
function showTransaksi($q = null, $dari = null, $sampai = null){
    $strQuery = "SELECT A.*, B.nama_tamu, B.id_agen, C.nama_agen,D.proses as proses_transaksi
FROM transaksi AS A
INNER JOIN tamu AS B ON A.id_tamu = B.id_tamu
INNER JOIN agen AS C ON B.id_agen = C.id_agen 
INNER JOIN master_proses_visa AS D ON A.proses = D.id
";
    $strQuery .= " WHERE 1 ";
    if($q){
        $strQuery .= " AND (
        A.id_transaksi = '{$q}' OR 
        A.id_tamu = '{$q}' OR 
        B.id_agen = '{$q}' OR 
        A.keterangan_transaksi like '%{$q}%' OR 
        A.tipe_transaksi like '%{$q}%' OR 
        B.nama_tamu like '%{$q}%' OR 
        C.nama_agen like '%{$q}%')";
    }

    if($dari && $sampai){
        $strQuery .= " AND A.tanggal BETWEEN '{$dari}' AND '{$sampai}' ";
    }else if(!$dari && $sampai){
        $strQuery .= " AND A.tanggal <= '{$sampai}'";
    }else if($dari && !$sampai){
        $strQuery .= " AND A.tanggal >= '{$dari}'";
    }

    $strQuery .= " ORDER BY A.tanggal DESC";
    $query = query($strQuery);
    return $query;
}

/*===========TRANSAKSI=============*/
/*===========TRANSAKSI MASUK=============*/
/**
 * Menampilkan Transaksi Masuk
 * @param string $q
 * @param string $dari
 * @param string $sampai
 * @return mysqli_result
 */

function showTransaksiMasuk($q = null, $dari = null, $sampai = null){
    $strQuery = "SELECT A.*, B.nama_tamu, B.id_agen, C.nama_agen,D.proses as proses_transaksi
FROM transaksi AS A
INNER JOIN tamu AS B ON A.id_tamu = B.id_tamu
INNER JOIN agen AS C ON B.id_agen = C.id_agen 
INNER JOIN master_proses_visa AS D ON A.proses = D.id
";
    $strQuery .= " WHERE tipe_transaksi = 'Masuk'";
    if($q){
        $strQuery .= " AND (
        A.id_transaksi = '{$q}' OR 
        A.id_tamu = '{$q}' OR 
        B.id_agen = '{$q}' OR 
        A.keterangan_transaksi like '%{$q}%' OR 
        A.tipe_transaksi like '%{$q}%' OR 
        B.nama_tamu like '%{$q}%' OR 
        C.nama_agen like '%{$q}%')";
    }

    if($dari && $sampai){
        $strQuery .= " AND A.tanggal BETWEEN '{$dari}' AND '{$sampai}' ";
    }else if(!$dari && $sampai){
        $strQuery .= " AND A.tanggal <= '{$sampai}'";
    }else if($dari && !$sampai){
        $strQuery .= " AND A.tanggal >= '{$dari}'";
    }

    $strQuery .= " ORDER BY A.tanggal DESC";
    $query = query($strQuery);
    return $query;
}

/**
 * @param $id
 * @return mysqli_result
 */
function showTransaksiMasukByID($id){
    $strQuery = "SELECT A.*, B.nama_tamu, B.id_agen, C.nama_agen,D.proses as proses_transaksi
FROM transaksi AS A
INNER JOIN tamu AS B ON A.id_tamu = B.id_tamu
INNER JOIN agen AS C ON B.id_agen = C.id_agen 
INNER JOIN master_proses_visa AS D ON A.proses = D.id
";
    $strQuery .= " WHERE tipe_transaksi = 'Masuk' AND A.id_transaksi = '{$id}'";
    $strQuery .= " ORDER BY A.tanggal DESC";
    $query = query($strQuery);
    return $query;
}

function showTransaksiMasukByTamuID($id){
    $strQuery = "SELECT A.*, B.nama_tamu, B.id_agen, C.nama_agen,D.proses as proses_transaksi
FROM transaksi AS A
INNER JOIN tamu AS B ON A.id_tamu = B.id_tamu
INNER JOIN agen AS C ON B.id_agen = C.id_agen 
INNER JOIN master_proses_visa AS D ON A.proses = D.id
";
    $strQuery .= " WHERE tipe_transaksi = 'Masuk' AND A.id_tamu = '{$id}'";
    $strQuery .= " ORDER BY A.tanggal DESC";
    $query = query($strQuery);
    return $query;
}

function showTransaksiKeluar($q = null, $dari = null, $sampai = null){
    $strQuery = "SELECT * FROM transaksi ";
    $strQuery .= " WHERE tipe_transaksi = 'Keluar'";
    if($q){
        $strQuery .= " AND (
        id_transaksi = '{$q}' OR          
        keterangan_transaksi like '%{$q}%'";
    }

    if($dari && $sampai){
        $strQuery .= " AND tanggal BETWEEN '{$dari}' AND '{$sampai}' ";
    }else if(!$dari && $sampai){
        $strQuery .= " AND tanggal <= '{$sampai}'";
    }else if($dari && !$sampai){
        $strQuery .= " AND tanggal >= '{$dari}'";
    }

    $strQuery .= " ORDER BY tanggal DESC";
    $query = query($strQuery);
    return $query;
}

function getTransaksiIDByIDProsesAndIDTamu($idProses, $idTamu){
    $strQuery = "SELECT A.id_transaksi FROM transaksi AS A
WHERE A.status = 'Belum Lunas' AND A.id_tamu = '{$idTamu}' AND A.proses = {$idProses}" ;
    $query = query($strQuery);
    if($query){
        $hutang = $query->fetch_assoc();
        return $hutang['id_transaksi'];
    }else{
        return 0;
    }
}

function updateTransaksiTunggakanByID($id, $sisa_pembayaran, $status){
    $strQuery= "UPDATE transaksi SET sisa_pembayaran = $sisa_pembayaran, status = '$status' WHERE id_transaksi = '$id'";
    $query = query($strQuery);
    if($query){
        return true;
    }else{
        return false;
    }
}

/**
 * @param $idTamu
 * @param $total
 * @param $keterangan
 * @param $proses
 * @param null $induk
 * @param null $jenis
 * @param string $status
 * @param string $tipe
 * @return string
 */
function insertTransaksi($idTamu, $total, $sisa_pembayaran, $keterangan,$proses,$induk = null, $jenis = null, $status = 'Lunas', $tipe = 'Masuk'){
    global $mysqli;
    $induk = ($induk)? $induk: null;
    $jenis = ($jenis)? $jenis:0;
    $status = ($status =='DP')? 'Belum Lunas':$status;
    $id = autoNumber('id_transaksi','TR','transaksi');
    $date = date('Y-m-d');
    $identity = getIdentity();
    $idUser = $identity->user_id;
    $prepareSQLTrans = "INSERT INTO transaksi (id_transaksi,id_tamu, tanggal, keterangan_transaksi, total_transaksi,
            sisa_pembayaran, proses, induk, jenis_transaksi, status, tipe_transaksi, id_user) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    try{
        $statementTrans = $mysqli->prepare($prepareSQLTrans);
        $statementTrans->bind_param('ssssddisissi',$id,$idTamu,$date,$keterangan,$total,
            $sisa_pembayaran,$proses,$induk,$jenis,$status,$tipe, $idUser);
        $statementTrans->execute();

    }catch (mysqli_sql_exception $ex){
        print_r($ex->getMessage());
        die();
    }
//    query("INSERT INTO transaksi VALUES ('$id','$idTamu','$date','$keterangan',$total,$proses,$idUser,$induk,$jenis,'$status','$tipe')");
    return $id;
}
/*===========TRANSAKSI MASUK=============*/
/*===========HUTANG PIUTANG=============*/
function getPiutang($q = null){
    $strQuery = "SELECT
A.id_transaksi,
A.id_tamu,
B.nama_tamu,
A.tanggal,
A.keterangan_transaksi,
A.sisa_pembayaran,
A.proses as id_proses,
master_proses_visa.proses
FROM
transaksi AS A
INNER JOIN tamu AS B ON A.id_tamu = B.id_tamu
LEFT JOIN master_proses_visa ON B.proses = master_proses_visa.id
WHERE A.status = 'Belum Lunas'";
    if($q){
        $strQuery .= " AND (
        A.id_transaksi = '{$q}' OR         
        A.id_tamu = '{$q}' OR 
        B.id_agen = '{$q}' OR 
        A.keterangan_transaksi like '%{$q}%' OR         
        B.nama_tamu like '%{$q}%' OR 
        C.nama_agen like '%{$q}%')";
    }
//echo $strQuery;
    $query = query($strQuery);
    return $query;
}

function getPiutangByIDTamu($id_tamu){
    $strQuery = "SELECT
A.id_transaksi,
A.id_tamu,
B.nama_tamu,
A.tanggal,
A.keterangan_transaksi,
A.sisa_pembayaran,
A.proses as id_proses,
master_proses_visa.proses
FROM
transaksi AS A
INNER JOIN tamu AS B ON A.id_tamu = B.id_tamu
LEFT JOIN master_proses_visa ON B.proses = master_proses_visa.id
WHERE A.status = 'Belum Lunas' AND A.id_tamu = '{$id_tamu}'" ;
    $query = query($strQuery);
    return $query;
}
function getPiutangByIDTamuAndProses($id_tamu, $idProses){
    $strQuery = "SELECT A.sisa_pembayaran FROM transaksi AS A
WHERE A.status = 'Belum Lunas' AND A.id_tamu = '{$id_tamu}' AND A.proses = {$idProses}" ;
    $query = query($strQuery);
    if($query){
        $hutang = $query->fetch_assoc();
       return $hutang['sisa_pembayaran'];
    }else{
        return 0;
    }

}
/*===========HUTANG PIUTANG=============*/
/*===========JURNAL UMUM=============*/

/**
 * @param $dari
 * @param $sampai
 * @param null $q
 * @return mysqli_result
 */
function showJurnalUmum($dari, $sampai, $q = null, $idTransaksi = null){

    if($dari && $sampai){
        $filterTgl = " WHERE tr.tanggal BETWEEN '{$dari}' AND '{$sampai}'";
    }elseif (!$dari && $sampai){
        $filterTgl = " WHERE tr.tanggal <= '{$sampai}'";
    }elseif ($dari && !$sampai){
        $filterTgl = " WHERE tr.tanggal >= '{$dari}'";
    }elseif (!$dari && !$sampai){
        $filterTgl = " WHERE 1";
    }

    $strQuery = "SELECT trn.id_transaksi, a.kode_rekening, a.nama_rekening, trn.tanggal, trn.nama_tamu, trn.nama_agen, 
  trn.keterangan_transaksi, dj.debit, dj.credit, a.klasifikasi, dj.posting
FROM
jurnal_detail AS dj
LEFT JOIN jurnal AS j ON dj.id_jurnal = j.id
LEFT JOIN akun AS a ON dj.kode_akun = a.kode_rekening
LEFT JOIN (
  SELECT tr.*, t.nama_tamu, agn.nama_agen FROM transaksi tr 
  LEFT JOIN tamu t ON tr.id_tamu = t.id_tamu
  LEFT JOIN agen agn ON t.id_agen = agn.id_agen
  {$filterTgl}
) AS trn ON trn.id_transaksi = j.id_transaksi ";

    if($dari && $sampai){
        $strQuery .= " WHERE trn.tanggal BETWEEN '{$dari}' AND '{$sampai}'";
    }elseif (!$dari && $sampai){
        $strQuery .= " WHERE trn.tanggal <= '{$sampai}'";
    }elseif ($dari && !$sampai){
        $strQuery .= " WHERE trn.tanggal >= '{$dari}'";
    }elseif (!$dari && !$sampai){
        $strQuery .= " WHERE 1";
    }

    if($idTransaksi){
        $strQuery .= " trn.id_transaksi = '$idTransaksi' ";
    }

    if($q){
        $strQuery .= " AND a.nama_rekening LIKE '%{$q}%' OR trn.keterangan_transaksi LIKE '%{$q}%' 
        OR trn.nama_tamu LIKE '%{$q}%' OR trn.nama_agen = '%{$q}%'";
    }

    $strQuery .= " ORDER BY trn.tanggal DESC, dj.debit DESC , dj.id ASC";
//    echo $strQuery;
    $query = query($strQuery);
    return $query;
}

function showCountNotPosting($dari, $sampai, $q = null, $idTransaksi = null){

    if($dari && $sampai){
        $filterTgl = " WHERE tr.tanggal BETWEEN '{$dari}' AND '{$sampai}'";
    }elseif (!$dari && $sampai){
        $filterTgl = " WHERE tr.tanggal <= '{$sampai}'";
    }elseif ($dari && !$sampai){
        $filterTgl = " WHERE tr.tanggal >= '{$dari}'";
    }elseif (!$dari && !$sampai){
        $filterTgl = " WHERE 1";
    }

    $strQuery = "SELECT COUNT(trn.id_transaksi) as count
FROM
jurnal_detail AS dj
LEFT JOIN jurnal AS j ON dj.id_jurnal = j.id
LEFT JOIN akun AS a ON dj.kode_akun = a.kode_rekening
LEFT JOIN (
  SELECT tr.*, t.nama_tamu, agn.nama_agen FROM transaksi tr 
  LEFT JOIN tamu t ON tr.id_tamu = t.id_tamu
  LEFT JOIN agen agn ON t.id_agen = agn.id_agen
  {$filterTgl}
) AS trn ON trn.id_transaksi = j.id_transaksi ";

    $strQuery .= " WHERE dj.posting = 0";

    if($dari && $sampai){
        $strQuery .= " AND trn.tanggal BETWEEN '{$dari}' AND '{$sampai}'";
    }elseif (!$dari && $sampai){
        $strQuery .= " WHERE trn.tanggal <= '{$sampai}'";
    }elseif ($dari && !$sampai){
        $strQuery .= " WHERE trn.tanggal >= '{$dari}'";
    }elseif (!$dari && !$sampai){

    }

    if($idTransaksi){
        $strQuery .= " trn.id_transaksi = '$idTransaksi' ";
    }

    if($q){
        $strQuery .= " AND a.nama_rekening LIKE '%{$q}%' OR trn.keterangan_transaksi LIKE '%{$q}%' 
        OR trn.nama_tamu LIKE '%{$q}%' OR trn.nama_agen = '%{$q}%'";
    }
//    echo $strQuery;
    $query = query($strQuery);
    $resCount = 0;
    if($query){
        $count = $query->fetch_row();
        $resCount = $count[0];
    }
    return $resCount;
}

function HitungMutasi($dari, $sampai, $q = null, $idTransaksi = null){

    if($dari && $sampai){
        $filterTgl = " WHERE tr.tanggal BETWEEN '{$dari}' AND '{$sampai}'";
    }elseif (!$dari && $sampai){
        $filterTgl = " WHERE tr.tanggal <= '{$sampai}'";
    }elseif ($dari && !$sampai){
        $filterTgl = " WHERE tr.tanggal >= '{$dari}'";
    }elseif (!$dari && !$sampai){
        $filterTgl = " WHERE 1";
    }

    $strQuery = "SELECT j.id_transaksi, dj.kode_akun, dj.debit, dj.credit, dj.id
FROM
jurnal_detail AS dj
LEFT JOIN jurnal AS j ON dj.id_jurnal = j.id
LEFT JOIN akun AS a ON dj.kode_akun = a.kode_rekening
LEFT JOIN (
  SELECT tr.*, t.nama_tamu, agn.nama_agen FROM transaksi tr 
  LEFT JOIN tamu t ON tr.id_tamu = t.id_tamu
  LEFT JOIN agen agn ON t.id_agen = agn.id_agen
  {$filterTgl}
) AS trn ON trn.id_transaksi = j.id_transaksi ";

    $strQuery .= " WHERE dj.posting = 0";

    if($dari && $sampai){
        $strQuery .= " AND trn.tanggal BETWEEN '{$dari}' AND '{$sampai}'";
    }elseif (!$dari && $sampai){
        $strQuery .= " WHERE trn.tanggal <= '{$sampai}'";
    }elseif ($dari && !$sampai){
        $strQuery .= " WHERE trn.tanggal >= '{$dari}'";
    }elseif (!$dari && !$sampai){

    }

    if($idTransaksi){
        $strQuery .= " trn.id_transaksi = '$idTransaksi' ";
    }

    if($q){
        $strQuery .= " AND a.nama_rekening LIKE '%{$q}%' OR trn.keterangan_transaksi LIKE '%{$q}%' 
        OR trn.nama_tamu LIKE '%{$q}%' OR trn.nama_agen = '%{$q}%'";
    }
    $query = query($strQuery);

    if($query){
        while ($rowMutasi = $query->fetch_object()){
            $rDebet = $rowMutasi->debit;
            $rCredit = $rowMutasi->credit;
            $rAkun = $rowMutasi->kode_akun;
            $rTrans = $rowMutasi->id_transaksi;
            $rID = $rowMutasi->id;

            $strUpdt = "UPDATE akun SET debit = IFNULL(debit,0)+ {$rDebet}, kredit = IFNULL(kredit,0) +{$rCredit} WHERE kode_rekening = '{$rAkun}'";
            query($strUpdt);
            ////////////////////////// UBAH STATUS POSTING //////////////////////////////
            query("UPDATE jurnal_detail SET posting = 1 WHERE id={$rID}");
        }
    }

    return "Posting Transaksi Berhasil Di Proses";
}

function showJournal($q = null, $dari = null, $sampai = null){
    $strQuery = "SELECT * FROM jurnal ";
    if($q){
        $strQuery .= " AND (
        id_transaksi = '{$q}' OR          
        keterangan like '%{$q}%'";
    }

    if($dari && $sampai){
        $strQuery .= " AND tanggal BETWEEN '{$dari}' AND '{$sampai}' ";
    }else if(!$dari && $sampai){
        $strQuery .= " AND tanggal <= '{$sampai}'";
    }else if($dari && !$sampai){
        $strQuery .= " AND tanggal >= '{$dari}'";
    }

    $strQuery .= " ORDER BY tanggal DESC";
    $query = query($strQuery);
    return $query;
}

/**
 * @param $id_transaksi
 * @param $keterangan
 * @param $tanggal
 * @param array $detail
 */
function insertJournal($id_transaksi, $keterangan, $tanggal, $detail = []){
     $journal = insert("INSERT INTO jurnal (id_transaksi, keterangan, tanggal) VALUES ('$id_transaksi','$keterangan','$tanggal')");
     if(is_array($detail)){
         foreach ($detail as $item){
             $id_jurnal = $journal;
             $kode_akun = $item['kode_akun'];
             $debit = $item['debit'];
             $credit = $item['credit'];
             insert("INSERT INTO jurnal_detail(id_jurnal, kode_akun, debit, credit) 
              VALUES ('$id_jurnal','$kode_akun',$debit, $credit)");
         }
     }

}
/*===========JURNAL UMUM=============*/




/*=============================DAFTAR QUERY==========================*/
