<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/25/2017
 * Time: 9:22 AM
 */

/**
 * @return string
 */
function baseUrl(){
    $currentPath = $_SERVER['PHP_SELF'];
    return sprintf(
        "%s://%s%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'],
        dirname($currentPath) . '/'
    );
}

/**
 * @param $number
 * @return string
 */
function angkaIndo($number){
//    if($number<0){
//        $innum = abs($number);
//        $result = number_format($innum,2,",",".") * -1;
//    }else {
//        $result = number_format($number,2,",",".");
//    }
    $result = number_format($number,2,",",".");
    return $result;
}

/**
 * @param $date
 * @param bool $short
 * @return string
 */
function TanggalIndo($date,$short = true){
    if($short){
        $BulanIndo = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agst", "Sept", "Okt", "Nov", "Des");
    }else{
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    }

    $tahun = substr($date, 0, 4);
    $bulan = substr($date, 5, 2);
    $tgl   = substr($date, 8, 2);

    $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
    return($result);
}

/**
 * @param $date
 * @return string
 */
function ymdToDmy($date){
    list($year, $month, $day) = explode("-", $date);
    $ymd = "$day-$month-$year";
    return $ymd;
}

/**
 * @param $date
 * @param string $delimiter
 * @return string
 */
function dmyToYmd($date,$delimiter = "-"){
    list($day, $month, $year) = explode($delimiter, $date);
    $ymd = "$year-$month-$day";
    return $ymd;
}

/**
 * @param $nilai
 * @return mixed
 */
function stringToNumber($nilai){
    $nilai = str_replace(',','.',str_replace('.','',$nilai));
    return $nilai;
}

/**
 * @param $id
 * @param $prefix
 * @param $table
 * @return string
 */
function autoNumber($id,$prefix , $table){
    $querytext = 'SELECT MAX(RIGHT('.$id.', 4)) as max_id FROM '.$table.' ORDER BY '.$id;
    /**
     * @var $query mysqli_result
     **/
    $query = query($querytext);
    if($query){
        $data = $query->fetch_assoc();
        $id_max = $data['max_id'];
        $sort_num = (int) substr($id_max, 1, 4);
        $sort_num++;
    }else{
        $sort_num =1;
    }

    $new_code = $prefix . sprintf("%04s", $sort_num);
    return $new_code;
}

/**
 * @param $path
 * @return string
 */
function url($path){
    return baseUrl() . $path;
}
function urlAsset($path){
    return baseUrl() . 'assets/' . $path;
}

/**
 *
 */
function start_session(){
//    error_reporting(0);
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }else{
        session_write_close();
        session_start();
    }
}

/**
 * @return bool
 */
function checkAksesLogin(){
    start_session();
    if(isset($_SESSION['accApp']['akses'])){
        return $_SESSION['accApp']['akses'];
    }else{
        return false;
    }
}

/**
 * Get user identity
 * @return bool|object
 */
function getIdentity(){
    $akses = checkAksesLogin();
    if($akses){
        return (object) $_SESSION['accApp'];
    }
    return false;
}

/**
 * @param $jenis_id
 * @return mixed
 */
function jenisTransaksi($jenis_id){
    $trans = [
        0=> 'Pendaftaran Visa',
        1=> 'Perpanjangan Visa',
        2=> 'Pelunasan Visa',
    ];
    if($jenis_id !=null){
        return $trans[$jenis_id];
    }
    return null;
}

/**
 * @param $arrCss
 * @return string
 */
function loadCss($arrCss){
    $template = '<link href="{css}" rel="stylesheet">';
    $listCss="";
    if(is_array($arrCss)){
        foreach ($arrCss as $row){
            $listCss .= strtr($template,[
                '{css}' => urlAsset($row)
            ]);
        }
    }else{
        $listCss .= strtr($template,[
            '{css}' => urlAsset($arrCss)
        ]);
    }

    return $listCss;
}

/**
 * @param $arrJs
 * @return string
 */
function loadJs($arrJs){
    $template = '<script src="{js}"></script>';
    $listJs="";
    if(is_array($arrJs)){
        foreach ($arrJs as $row){
            $listJs .= strtr($template,[
                '{js}' => urlAsset($row)
            ]);
        }
    }else{
        $listJs .= strtr($template,[
            '{css}' => urlAsset($arrJs)
        ]);
    }

    return $listJs;
}

/**
 * @param $path
 * @return string
 */
function getImage($path){
    return urlAsset("img/{$path}");
}