<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/25/2017
 * Time: 10:46 AM
 */

?>

<div class="am-sideleft">
    <ul class="nav am-sideleft-menu">
        <li class="nav-item">
            <a href="<?= url('home.php')?>" class="nav-link active">
                <i class="icon ion-ios-home-outline"></i>
                <span>Menu Utama</span>
            </a>
        </li><!-- nav-item -->
        <li class="nav-item">
            <a href="<?= url('index.php?page=data-tamu')?>" class="nav-link active">
                <i class="icon ion-android-contacts"></i>
                <span>Data Tamu</span>
            </a>
        </li><!-- nav-item -->
        <li class="nav-item">
            <a href="<?= url('index.php?page=data-agen')?>" class="nav-link active">
                <i class="icon ion-ios-people-outline"></i>
                <span>Data Agen</span>
            </a>
        </li><!-- nav-item -->
        <li class="nav-item">
            <a href="<?= url('index.php?page=data-akun')?>" class="nav-link active">
                <i class="icon ion-ios-bookmarks-outline"></i>
                <span>Data Akun</span>
            </a>
        </li><!-- nav-item -->
        <li class="nav-item">
            <a href="<?= url('index.php?page=data-kewarganegaraan')?>" class="nav-link active">
                <i class="fa fa-globe"></i>
                <span>Data Kewarganegaraan</span>
            </a>
        </li><!-- nav-item -->
        <li class="nav-item">
            <a href="<?= url('index.php?page=data-proses')?>" class="nav-link active">
                <i class="icon ion-shuffle"></i>
                <span>Data Jasa</span>
            </a>
        </li><!-- nav-item -->
        <li class="nav-item">
            <a href="" class="nav-link with-sub active">
                <i class="icon ion-ios-book-outline"></i>
                <span>Transaksi</span>
            </a>
            <ul class="nav-sub">
                <li class="nav-item"><a href="<?= url('index.php?page=transaksi-masuk')?>" class="nav-link">Transaksi Masuk</a></li>
                <li class="nav-item"><a href="<?= url('index.php?page=transaksi-keluar')?>" class="nav-link">Transaksi Keluar</a></li>
                <li class="nav-item"><a href="<?= url('index.php?page=posting')?>" class="nav-link">Posting</a></li>
            </ul>
        </li><!-- nav-item -->
        <li class="nav-item">
            <a href="" class="nav-link with-sub active">
                <i class="icon ion-ios-book-outline"></i>
                <span>Laporan</span>
            </a>
            <ul class="nav-sub">
                <li class="nav-item"><a href="<?= url('index.php?page=laporan-jurnal-umum')?>" class="nav-link">Laporan Jurnal Umum</a></li>
                <li class="nav-item"><a href="<?= url('index.php?page=laporan-hutang-piutang')?>" class="nav-link">Laporan Hutang/Piutang</a></li>
                <li class="nav-item"><a href="<?= url('index.php?page=laporan-laba-rugi')?>" class="nav-link">Laporan Laba Rugi</a></li>
                <li class="nav-item"><a href="<?= url('index.php?page=laporan-data-agen')?>" class="nav-link">Laporan Data Agen</a></li>
                <li class="nav-item"><a href="<?= url('index.php?page=laporan-data-tamu')?>" class="nav-link">Laporan Data Tamu</a></li>
                <li class="nav-item"><a href="<?= url('index.php?page=laporan-data-akun')?>" class="nav-link">Laporan Data Akun</a></li>
            </ul>
        </li><!-- nav-item -->
        <li class="nav-item">
            <a href="<?= url('index.php?page=grafik-keuangan')?>" class="nav-link active">
                <i class="icon ion-ios-pie-outline"></i>
                <span>Grafik Keuangan Perusahaan</span>
            </a>
        </li><!-- nav-item -->
    </ul>
</div><!-- am-sideleft -->

