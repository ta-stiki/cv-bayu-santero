<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/25/2017
 * Time: 10:43 AM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">
    <title>CV. BAYU SANTERO</title>
    <!-- vendor css -->
    <?= loadCss([
        'lib/font-awesome/css/font-awesome.css',
        'lib/Ionicons/css/ionicons.css',
        'lib/perfect-scrollbar/css/perfect-scrollbar.css',
        'lib/jquery-toggles/toggles-full.css',
        'lib/select2/css/select2.min.css',
        'lib/rickshaw/rickshaw.min.css',
        'css/amanda.css',
        'css/flags/flags.min.css',
        'css/print.css',
    ])?>
    <?= loadJs([
        'lib/jquery/jquery.js',
        'lib/popper.js/popper.js',
        'lib/bootstrap/bootstrap.js',
        'lib/jquery-inputmask/jquery.inputmask.bundle.js',
    ])?>
</head>

<body>
