<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:07 PM
 */
?>
<div class="row row-sm pd-t-20">
    <div class="col-lg-4 pd-b-40">
        <a href="<?= url('index.php?page=transaksi-masuk')?>">
            <div class="card bd-0 wd-xs-300 ">
                <div class="tx-32 tx-center pd-t-20">
                    <i class="fa fa-download"></i>
                </div>
                <div class="card-body bd-t-0 tx-center ">
                    <h6 class="mg-b-3 tx-dark">Transaksi Masuk</h6>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 pd-b-40">
        <a href="<?= url('index.php?page=transaksi-keluar')?>">
            <div class="card bd-0 wd-xs-300 ">
                <div class="tx-32 tx-center pd-t-20">
                    <i class="fa fa-upload"></i>
                </div>
                <div class="card-body bd-t-0 tx-center ">
                    <h6 class="mg-b-3 tx-dark">Transaksi Keluar</h6>
                </div>
            </div>
        </a>
    </div>
</div>