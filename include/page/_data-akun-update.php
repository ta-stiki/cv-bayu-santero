<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:20 AM
 */
$dataID = isset($_GET['id'])? escape($_GET['id']):null;
$data_tamu = new stdClass();
if($dataID){
    $data_tamu = showDataAkunByID($dataID);

    $nama_rekening = $data_tamu->nama_rekening;
    $klasifikasi = $data_tamu->klasifikasi;
}
$mode = 'Update';
?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-title text-center">Update Data Akun</h6>
    <?php include_once __DIR__ . '/_data-akun-form.php';?>
</div>

