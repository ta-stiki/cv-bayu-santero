<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:08 PM
 */

$q = isset($_POST['q'])? escape($_POST['q']): null;
$dataTamu = showDataTamu($q);

?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">LAPORAN DATA TAMU</h6>
    <div class="row pd pd-b-5">
        <div class="col-md-6">
        </div>
        <div class="col-md-6">
            <form action="" method="post">
                <div class="input-group">
                    <input name="q" type="text" class="form-control" placeholder="Cari" value="<?= $q?>">
                    <span class="input-group-btn">
                    <button class="btn bd bg-white tx-gray-600"><i class="icon ion-search"></i></button>
                </span>
                </div>
            </form>
        </div>
    </div>
    <div id='DivIdToPrint' class="table-responsive">
        <table class="table table-hover table-bordered table-primary mg-b-0">
            <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">ID Tamu</th>
                <th class="text-center">Nama Tamu</th>
                <th class="text-center">Jenis Kelamin</th>
                <th class="text-center">Kewarganegaraan</th>
                <th class="text-center">Proses</th>
                <th class="text-center">No HP</th>
                <th class="text-center">Agen</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($dataTamu){
                $i=0;
                /**
                 * @var $dataTamu mysqli_result
                 **/
                while($item = $dataTamu->fetch_object()){
                    $i++;
                    ?>
                    <tr>
                        <td><?= $i?></td>
                        <td><?= $item->id_tamu?></td>
                        <td><?= $item->nama_tamu?></td>
                        <td><?= $item->jenis_kelamin?></td>
                        <td><?= $item->kewarganegaraan?></td>
                        <td><?= getNameProsesVisa($item->proses)?></td>
                        <td><?= $item->no_hp?></td>
                        <td><?= $item->nama_agen?></td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div><!-- table-responsive -->
    <div class="row pd pd-t-5">
        <div class="col-md-6">
            <button onclick="printDiv();" class="btn btn-dark"><i class="fa fa-print"></i> Cetak </button>
        </div>
    </div>
</div>
<script>
    function printDiv()
    {

        var divToPrint=document.getElementById('DivIdToPrint');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html>' +
            '<link href="<?= url('assets/css/print.css')?>" rel="stylesheet">' +
            '<body onload="window.print()">'+
            '<h1 style="text-align: center">CV BAYU SANTERO</h1>' +
            '<p style="text-align: center">JL. Raya Semer NO 26 Kerobokan Kuta-Badung</p>' +
            '<h3 style="text-align: center">Laporan Daftar Tamu</h3>' +
            '<br>' +

            divToPrint.innerHTML +
            '</body>' +
            '</html>');
        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

    }
</script>