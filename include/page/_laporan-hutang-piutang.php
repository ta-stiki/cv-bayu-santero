<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:08 PM
 */


$q = isset($_POST['q'])? escape($_POST['q']): null;
$dataTamu = getPiutang($q);

?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">Laporan Hutang Piutang</h6>
    <div class="row pd pd-b-5">
        <div class="col-md-6">
        
        </div>
        <div class="col-md-6">
            <form action="" method="post">
                <div class="input-group">
                    <input name="q" type="text" class="form-control" placeholder="Cari" value="<?= $q?>">
                    <span class="input-group-btn">
                        <button class="btn bd bg-white tx-gray-600"><i class="icon ion-search"></i></button>                    
                    </span>
                </div>
            </form>
        </div>            
    </div>
    <?php
    if($dataTamu){
        ?>
        <button onclick="printDiv();" class="btn btn-dark"><i class="fa fa-print"></i> Cetak </button>
        <?php
    }
    ?>    
    <div id="DivIdToPrint" class="table-responsive">
        <table class="table table-hover table-bordered table-primary mg-b-0">
            <thead>
            <tr>
                <th class="text-center">Kode Transaksi</th>
                <th class="text-center">ID Tamu</th>
                <th class="text-center">Nama Tamu</th>
                <th class="text-center">Keterangan Transaksi</th>
                <th class="text-center">Hutang</th>
                <th class="text-center">Piutang</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($dataTamu){
                $i=0;
                $totalPiutang = 0;
                while($item = $dataTamu->fetch_object()){
                    $totalPiutang += $item->sisa_pembayaran;
                    ?>
                    <tr>
                        <td ><?= $item->id_transaksi?></td>
                        <td ><?= $item->id_tamu?></td>
                        <td ><?= $item->nama_tamu?></td>
                        <td ><?= $item->keterangan_transaksi?></td>
                        <td class="text-right">0</td>
                        <td class="text-right"><?= angkaIndo($item->sisa_pembayaran)?></td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4" class="text-center"><b>Total</b></td>
                <td class="text-right"><b>0</b></td>
                <td class="text-right"><b><?= angkaIndo($totalPiutang)?></b></td>
            </tr>
            </tfoot>
        </table>
    </div><!-- table-responsive -->
</div>
<script>
    function printDiv()
    {

        var divToPrint=document.getElementById('DivIdToPrint');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html>' +
            '<link href="<?= url('assets/css/print.css')?>" rel="stylesheet">' +
            '<body onload="window.print()">'+
            '<h1 style="text-align: center">CV BAYU SANTERO</h1>' +
            '<h4 style="text-align: center">JL. Raya Semer NO 26 Kerobokan Kuta-Badung</h3>' +
            '<h3 style="text-align: center">LAPORAN HUTANG PIUTANG</h3>' +
            '<br>' +

            divToPrint.innerHTML +
            '</body>' +
            '</html>');
        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

    }
</script>