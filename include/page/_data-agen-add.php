<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:18 AM
 */
$data_tamu = new stdClass();
$nama_agen = '';
$alamat_agen = '';
$no_telp = '';
?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-title text-center">Tambah Data Agen</h6>
    <?php include_once __DIR__ . '/_data-agen-form.php';?>
</div>
