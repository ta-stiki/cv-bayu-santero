<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:18 AM
 * @var $mysqli mysqli
 *
 */
$listCountry = showListCountry();
$id = (isset($data_tamu->id_tamu))?$data_tamu->id_tamu:autoNumber('id_tamu','TD','tamu');

if(isset($_POST['aksi'])){
    $aksi = escape($_POST['aksi']);
    $nama_tamu = escape($_POST['nama_tamu']);
    $jenis_kelamin = escape($_POST['jenis_kelamin']);
    $kewarganegaraan = escape($_POST['kewarganegaraan']);
    $proses = escape($_POST['proses']);
    $no_hp = escape($_POST['no_hp']);
    $id_agen = escape($_POST['id_agen']);
    $jumlah_pembayaran = escape($_POST['jumlah_pembayaran']);
    $jumlah_dp = escape($_POST['jumlah_dp']);
    $sisa_pembayaran = escape($_POST['sisa_pembayaran']);
    $status_pembayaran = escape($_POST['status_pembayaran']);
    $jumlah_dp = escape($_POST['jumlah_dp']);

    switch ($aksi){
        case 'data-tamu-update':
            $queryText = "UPDATE tamu
            SET
            nama_tamu = '$nama_tamu',
            jenis_kelamin = '$jenis_kelamin',
            kewarganegaraan = '$kewarganegaraan',
            proses = '$proses',
            no_hp = '$no_hp',
            id_agen = '$id_agen',
            jumlah_pembayaran = '$jumlah_pembayaran',
            status_pembayaran = '$status_pembayaran',
            jumlah_dp = '$jumlah_dp'
            WHERE
            id_tamu = '$id'
            ";
            query($queryText);
            break;
        case 'data-tamu-add':
            try{
                // insert transaksi
                $id_transaksi = autoNumber('id_transaksi', 'TR','transaksi');
                $date = date('Y-m-d');
                $namaProses = getNameProsesVisa($proses);
                $keterangan = "Pendaftaran $nama_tamu dengan proses visa $namaProses";
                switch ($status_pembayaran){
                    case 'DP':
                        $detailJournal = [
                            [
                                'kode_akun' => '4.1', //Pendapatan Usaha
                                'debit' => 0,
                                'credit' => $jumlah_pembayaran
                            ],
                            [
                                'kode_akun' => '1.1', //Kas
                                'debit' => $jumlah_dp,
                                'credit' => 0
                            ],
                            [
                                'kode_akun' => '1.2', //Piutang Usaha
                                'debit' => $sisa_pembayaran,
                                'credit' => 0
                            ],

                        ];
                        break;
                    case 'Lunas':
                        $jumlah_dp = 0;
                        $sisa_pembayaran=0;
                        $detailJournal = [
                            [
                                'kode_akun' => '4.1', //Pendapatan Usaha
                                'debit' => 0,
                                'credit' => $jumlah_pembayaran
                            ],
                            [
                                'kode_akun' => '1.1', //Kas
                                'debit' => $jumlah_pembayaran,
                                'credit' => 0
                            ],
                        ];
                        break;
                    case 'Belum Lunas':
                        $detailJournal = [
                            [
                                'kode_akun' => '4.1', //Pendapatan Usaha
                                'debit' => 0,
                                'credit' => $jumlah_pembayaran
                            ],
                            [
                                'kode_akun' => '1.2', //Piutang Usaha
                                'debit' => $jumlah_pembayaran,
                                'credit' => 0
                            ],
                        ];
                        break;
                }

                query("INSERT INTO tamu VALUES ('$id','$nama_tamu','$jenis_kelamin','$kewarganegaraan','$proses','$no_hp','$id_agen',$jumlah_pembayaran,'$status_pembayaran','$jumlah_dp')");
                $lastIDTamu = $id;

                $transaksi = insertTransaksi($lastIDTamu,$jumlah_pembayaran,$sisa_pembayaran,$keterangan,$proses,null,0,
                    $status_pembayaran);

                if($transaksi){
                    insertJournal($transaksi,$keterangan,$date,$detailJournal);
                }
            }catch (mysqli_sql_exception $ex){
                print_r($ex->getMessage());
            }


//            $prepareSQLTrans = "INSERT INTO transaksi (id_transaksi,id_tamu, tanggal, keterangan_transaksi, total_transaksi,
//            sisa_pembayaran, proses, induk, jenis_transaksi, status, tipe_transaksi) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
//            $statementTrans = $mysqli->prepare($prepareSQLTrans);
//            $statementTrans->bind_param('ssssddisiss',$id_transaksi,$lastIDTamu,$date,'Pendaftaran Proses ' . $namaProses,$jumlah_pembayaran,
//                $sisa_pembayaran,$proses,null,0,$status_pembayaran);
            break;

    }
    ?>
    <script>
        window.location = '<?= url('index.php?page=data-tamu')?>';
    </script>
    <?php
}
?>
<style>
    fieldset {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
        width:inherit;
        /*border-width:1px;*/
        /*padding:0 10px;*/
        /*border-style:groove;*/
        /*border-color:dimgray;*/
    }
    fieldset > legend {
        width: inherit;
        padding: 0px 10px 0px 10px;
        text-align: center;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="post">
            <input type="hidden" name="aksi" value="<?= $page?>">
            <fieldset>
                <!-- Form Name -->
                <div class="row">
                    <div class="col-md-12">
                        <p style="float: right;padding:20px">Tanggal: <?= date('d-m-Y')?></p>
                    </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="id_tamu">ID TAMU</label>
                    <div class="col-md-8">
                        <input readonly id="id_tamu" name="id_tamu" type="text" placeholder="ID Tamu" class="form-control input-md" required="" value="<?= $id?>">

                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="id_agen">Agen</label>
                    <div class="col-md-8">
                        <select required id="id_agen" name="id_agen" class="form-control select2">
                            <option value="">--Pilih Agen--</option>
                            <?php
                            /**
                             * @var $agen mysqli_result
                             **/
                            $agen = showDataAgen();
                            while ($rowAgen = $agen->fetch_object()){
                                $select = ($id_agen == $rowAgen->id_agen)? 'selected':null;
                                echo "<option value=\"{$rowAgen->id_agen}\" $select>{$rowAgen->nama_agen}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nama_tamu">Nama Tamu</label>
                    <div class="col-md-8">
                        <input required id="nama_tamu" name="nama_tamu" type="text" placeholder="Nama Tamu" class="form-control input-md" required="" value="<?= $nama_tamu?>">

                    </div>
                </div>

                <!-- Multiple Radios (inline) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="jenis_kelamin">Jenis Kelamin</label>
                    <div class="col-md-8">
                        <label class="radio-inline" for="jenis_kelamin-0">
                            <input type="radio" <?= ($jenis_kelamin == 'laki-laki')? 'checked':null?> name="jenis_kelamin" id="jenis_kelamin-0" value="laki-laki">
                            Laki-Laki
                        </label>
                        <label class="radio-inline" for="jenis_kelamin-1">
                            <input type="radio" name="jenis_kelamin" id="jenis_kelamin-1" value="perempuan" <?= ($jenis_kelamin == 'perempuan')? 'checked':null?>>
                            Perempuan
                        </label>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="kewarganegaraan">Kewarganegaraan</label>
                    <div class="col-md-8">
                        <select required name="kewarganegaraan" id="kewarganegaraan" name="kewarganegaraan" class="form-control input-md">
                            <option value=""></option>
                            <?php
                            /**
                             * @var $listCountry mysqli_result
                             **/
                            while ($rowC = $listCountry->fetch_object()){
                                $select = ($kewarganegaraan == $rowC->country_name)? 'selected':null;
                                echo "<option value=\"{$rowC->country_name}\" $select>{$rowC->country_name} <img src=\"blank.gif\" class=\"flag flag-". strtolower($rowC->country_code)."\" alt=\"$rowC->country_name\" /></option>";
                            }
                            ?>
                        </select>
<!--                        <input id="kewarganegaraan" name="kewarganegaraan" type="text" placeholder="Kewarganegaraan" class="form-control input-md" required="" value="--><?//=$kewarganegaraan?><!--">-->

                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="proses">Proses</label>
                    <div class="col-md-8">
                        <?php
                        $visa = showMasterProsesVisa();
                        ?>
                        <select required id="proses" name="proses" class="form-control">
                            <option value="">--Pilih Proses--</option>
                            <?php
                            while ($rowVisa = $visa->fetch_object()){
                                $select = ($proses == $rowVisa->id)? 'selected':null;
                                echo "<option value=\"{$rowVisa->id}\" $select>{$rowVisa->proses}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="no_hp">No HP</label>
                    <div class="col-md-8">
                        <input required id="no_hp" name="no_hp" type="text" placeholder="No Handphone" class="form-control input-md" value="<?= $no_hp?>">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="jumlah_pembayaran">Jumlah Pembayaran</label>
                    <div class="col-md-8">
                        <input required id="jumlah_pembayaran" name="jumlah_pembayaran" type="text" placeholder="Jumlah Pembayaran" class="form-control input-md numberinput" value="<?= $jumlah_pembayaran?>">

                    </div>
                </div>

                <!-- Multiple Radios (inline) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="status_pembayaran">Status Pembayaran</label>
                    <div class="col-md-8">
                        <label class="radio-inline" for="status_pembayaran-0">
                            <input <?= ($status_pembayaran == 'DP')? 'checked':null?> class="status_pembayaran" type="radio" name="status_pembayaran" id="status_pembayaran-0" value="DP" checked="checked">
                            DP
                        </label>
                        <label class="radio-inline" for="status_pembayaran-1">
                            <input <?= ($status_pembayaran == 'Lunas')? 'checked':null?> class="status_pembayaran" type="radio" name="status_pembayaran" id="status_pembayaran-1" value="Lunas">
                            Lunas
                        </label>
                        <label class="radio-inline" for="status_pembayaran-2">
                            <input <?= ($status_pembayaran == 'Belum Lunas')? 'checked':null?> class="status_pembayaran" type="radio" name="status_pembayaran" id="status_pembayaran-2" value="Belum Lunas">
                            Belum Lunas
                        </label>
                        <input id="jumlah_dp" name="jumlah_dp" type="text" placeholder="Jumlah DP" class="form-control input-md numberinput" style="display: none" value="<?= $jumlah_dp?>">
                        <input id="sisa_pembayaran" name="sisa_pembayaran" type="text" placeholder="Sisa Pembayaran" class="form-control input-md numberinput" style="display: none" >
                    </div>
                </div>
                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="button1id"></label>
                    <div class="col-md-8">
                        <button type="submit" id="btnBayar" name="button1id" class="btn btn-success">Save</button>
                        <a id="button2id" name="button2id" class="btn btn-danger" href="<?= url('index.php?page=data-tamu')?>">Cancel</a>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.select2').select2({
            minimumResultsForSearch: ''
        });

        $('#nama_tamu').autocomplete({
            source: function( request, response ) {
                $.get('<?= baseUrl() . 'include/actions.php?a=tamu_auto&q='?>' + request.term,function (result) {
                    response(JSON.parse(result));
                });
            }
        });

        $('#kewarganegaraan').select2({
            placeholder: "Pilih Kewarganegaraan",
            allowClear: false
        });

        var statusPembayaran = '<?= ($status_pembayaran)?$status_pembayaran:'DP'?>';
        if(statusPembayaran === 'DP'){
            $('#jumlah_dp').slideDown();
            $('#sisa_pembayaran').slideDown();
        }else{
            $('#jumlah_dp').slideUp();
            $('#sisa_pembayaran').slideUp();
        }

        $('#jumlah_pembayaran').on('change',function () {
            var jumlah_pembayaran =  $('#jumlah_pembayaran').inputmask('unmaskedvalue');
            var jumlah_dp =  $('#jumlah_dp').inputmask('unmaskedvalue');
            var sisa_pembayaran = jumlah_pembayaran - jumlah_dp;
            $('#sisa_pembayaran').val(sisa_pembayaran);
        });

        $('#jumlah_dp').on('blur',function () {
            var jumlah_pembayaran =  $('#jumlah_pembayaran').inputmask('unmaskedvalue');
            var jumlah_dp =  $('#jumlah_dp').inputmask('unmaskedvalue');
            if(jumlah_dp > jumlah_pembayaran) {
                alert("Maaf Jumlah DP Melebihi jumlah pembayaran");
                $('#btnBayar').prop('disabled',true);
            }else{
                $('#btnBayar').prop('disabled',false);
            }
            var sisa_pembayaran = jumlah_pembayaran - jumlah_dp;
            $('#sisa_pembayaran').val(sisa_pembayaran);
        });

        $('#proses').on('change',function () {
            var val = $(this).val();
            $.get('<?= baseUrl() . 'include/actions.php?a=tarif_proses&id='?>' + val,function (result) {

                $('#jumlah_pembayaran').val(result);
                var jumlah_pembayaran =  $('#jumlah_pembayaran').inputmask('unmaskedvalue');
                var jumlah_dp =  $('#jumlah_dp').inputmask('unmaskedvalue');
                var sisa_pembayaran = jumlah_pembayaran - jumlah_dp;
                $('#sisa_pembayaran').val(sisa_pembayaran);
            })
        });

        $('.status_pembayaran').on('change',function () {
            var statusPembayaran = $(this).val();
            if(statusPembayaran === 'DP'){
                $('#jumlah_dp').slideDown();
                $('#sisa_pembayaran').slideDown();
            }else{
                $('#jumlah_dp').slideUp();
                $('#sisa_pembayaran').slideUp();
            }
        })

    })
</script>
