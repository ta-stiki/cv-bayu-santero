<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:07 PM
 */

$q = isset($_POST['q'])? escape($_POST['q']): null;
$dataTamu = showDataAkun($q);

?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">DATA AKUN</h6>
    <div class="row pd pd-b-5">
        <div class="col-md-6">
            <a href="<?= url('index.php?page=data-akun-add')?>" class="btn btn-success mg-b-10"><i class="fa fa-plus-circle"></i> Tambah Data Akun</a>
        </div>
        <div class="col-md-6">
            <form action="" method="post">
                <div class="input-group">
                    <input name="q" type="text" class="form-control" placeholder="Cari" value="<?= $q?>">
                    <span class="input-group-btn">
                    <button class="btn bd bg-white tx-gray-600"><i class="icon ion-search"></i></button>
                </span>
                </div>
            </form>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered table-sm table-primary mg-b-0">
            <thead>
            <tr>
                <th class="text-center">No Akun</th>
                <th class="text-center">Nama Akun</th>
                <th class="text-center">Tipe Akun</th>
                <th class="text-center">Debit</th>
                <th class="text-center">Kredit</th>
                <th width="80px" class="text-center">Keterangan</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($dataTamu){
                /**
                 * @var $dataTamu mysqli_result
                 * @var $queryKlasifikasi mysqli_result
                 **/
                $queryKlasifikasi =  query("SELECT * FROM akun_klasifikasi");

                while ($row = $queryKlasifikasi->fetch_object()){
                    $i=0;
                    ?>
                    <tr>
                        <td colspan="7"><b><?= $row->deskripsi?></b></td>
                    </tr>
                    <?php
                    $dataTamu->data_seek(0);
                    while($item = $dataTamu->fetch_object()){
                        if($row->klasifikasi == $item->klasifikasi){
                            ?>
                            <tr>
                                <td class="text-center"><?= $item->kode_rekening?></td>
                                <td><?= $item->nama_rekening?></td>
                                <td><?= $row->deskripsi?></td>
                                <td class="text-right"><?= angkaIndo((($row->normal == 'Debit')?$item->debit:0))?></td>
                                <td class="text-right"><?= angkaIndo((($row->normal == 'Kredit')?$item->kredit:0))?></td>
                                <td class="text-center">
                                    <?php
                            if($identity->akses == 'admin'){
                                ?>
                                <a href="<?= url('index.php?page=data-akun-update&id='. $item->kode_rekening)?>" class="btn btn-outline-primary btn-icon mg-r-5">
                                    <div>
                                        <i class="fa fa-edit"></i>
                                    </div>
                                </a>
                                <?php
                            }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }

                    }
                }
            }else{
                echo "<tr><td colspan='7'>Data tidak ditemukan</td></tr>";
            }
            ?>
            </tbody>
        </table>
    </div><!-- table-responsive -->
</div>