<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:18 AM
 */
$id = (isset($data_tamu->id_agen))?$data_tamu->id_agen:autoNumber('id_agen','AG','agen');

if(isset($_POST['aksi'])){
    $aksi = escape($_POST['aksi']);
    $nama_agen = escape($_POST['nama_agen']);
    $alamat_agen = escape($_POST['alamat_agen']);
    $no_telp = escape($_POST['no_telp']);

    switch ($aksi){
        case 'data-agen-update':
            $queryText = "UPDATE agen
            SET
            nama_agen = '$nama_agen',
            alamat_agen = '$alamat_agen',
            no_telp = '$no_telp'            
            WHERE
            id_agen = '$id'
            ";
            query($queryText);
            break;
        case 'data-agen-add':
            query("INSERT INTO agen VALUES ('$id','$nama_agen','$alamat_agen','$no_telp')");
            break;

    }
    ?>
    <script>
        window.location = '<?= url('index.php?page=data-agen')?>';
    </script>
    <?php
}
?>
<style>
    fieldset {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
        width:inherit;
        /*border-width:1px;*/
        /*padding:0 10px;*/
        /*border-style:groove;*/
        /*border-color:dimgray;*/
    }
    fieldset > legend {
        width: inherit;
        padding: 0px 10px 0px 10px;
        text-align: center;
    }
</style>

<div class="row">
    <div class="col-md-12">

        <form class="form-horizontal" method="post">
            <input type="hidden" name="aksi" value="<?= $page?>">
            <fieldset>
                <div class="row">
                    <div class="col-md-12">
                        <p style="float: right;padding:20px">Tanggal: <?= date('d-m-Y')?></p>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="id_agen">ID Agen</label>
                    <div class="col-md-8">
                        <input readonly id="id_agen" name="id_agen" type="text" placeholder="ID Agen" class="form-control input-md" required="" value="<?= $id?>">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nama_agen">Nama Agen</label>
                    <div class="col-md-8">
                        <input id="nama_agen" name="nama_agen" type="text" placeholder="Nama Agen" class="form-control input-md" required="" value="<?= $nama_agen?>">

                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="alamat_agen">Alamat Agen</label>
                    <div class="col-md-8">
                        <textarea class="form-control" id="alamat_agen" name="alamat_agen" placeholder="Alamat Agen"><?= $alamat_agen?></textarea>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="no_telp">No Telp</label>
                    <div class="col-md-8">
                        <input id="no_telp" name="no_telp" type="text" placeholder="No Telp" class="form-control input-md" value="<?= $no_telp?>">

                    </div>
                </div>

                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="actionsave"></label>
                    <div class="col-md-8">
                        <button id="actionsave" name="actionsave" class="btn btn-success">Save</button>
                        <a id="button2id" name="button2id" class="btn btn-danger" href="<?= url('index.php?page=data-agen')?>">Cancel</a>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>