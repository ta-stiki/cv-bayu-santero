<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:08 PM
 */
$q = isset($_POST['q'])? escape($_POST['q']): null;
$dataTamu = showDataAkun($q);

?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">DATA AKUN</h6>
    <div class="row pd pd-b-5">
        <div class="col-md-6">
        </div>
        <div class="col-md-6">
            <form action="" method="post">
                <div class="input-group">
                    <input name="q" type="text" class="form-control" placeholder="Cari" value="<?= $q?>">
                    <span class="input-group-btn">
                    <button class="btn bd bg-white tx-gray-600"><i class="icon ion-search"></i></button>
                </span>
                </div>
            </form>
        </div>
    </div>
    <div id='DivIdToPrint' class="table-responsive">
        <table class="table table-hover table-bordered table-sm table-primary mg-b-0">
            <thead>
            <tr>
                <th class="text-center">No Akun</th>
                <th class="text-center">Nama Akun</th>
                <th class="text-center">Tipe Akun</th>
<!--                <th class="text-center">Debit</th>
                <th class="text-center">Kredit</th>-->
            </tr>
            </thead>
            <tbody>
            <?php
            if($dataTamu){
                /**
                 * @var $dataTamu mysqli_result
                 * @var $queryKlasifikasi mysqli_result
                 **/
                $queryKlasifikasi =  query("SELECT * FROM akun_klasifikasi");

                while ($row = $queryKlasifikasi->fetch_object()){
                    $i=0;
                    ?>
                    <tr>
                        <td colspan="7"><b><?= $row->deskripsi?></b></td>
                    </tr>
                    <?php
                    $dataTamu->data_seek(0);
                    while($item = $dataTamu->fetch_object()){
                        if($row->klasifikasi == $item->klasifikasi){
                            ?>
                            <tr>
                                <td class="text-center"><?= $item->kode_rekening?></td>
                                <td><?= $item->nama_rekening?></td>
                                <td><?= $row->deskripsi?></td>
<!--                                <td class="text-right"><?//= angkaIndo($item->awal_debet)?></td>
                                <td class="text-right"><?//= angkaIndo($item->awal_kredit)?></td>-->
                            </tr>
                            <?php
                        }

                    }
                }
            }
            ?>
            </tbody>
<!--            <tfoot>-->
<!--            <tr>-->
<!--                <td class="text-center" colspan="3"><b>Total</b></td>-->
<!--                <td class="text-right"><b>0</b></td>-->
<!--                <td class="text-right"><b>0</b></td>-->
<!--            </tr>-->
<!--            </tfoot>-->
        </table>
    </div><!-- table-responsive -->
    <div class="row pd pd-t-5">
        <div class="col-md-6">
            <button onclick="printDiv();" class="btn btn-dark"><i class="fa fa-print"></i> Cetak </button>
        </div>
    </div>
</div>
<script>
    function printDiv()
    {

        var divToPrint=document.getElementById('DivIdToPrint');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html>' +
            '<link href="<?= url('assets/css/print.css')?>" rel="stylesheet">' +
            '<body onload="window.print()">'+
            '<div id="header">'+
            '<h1 style="text-align: center">CV BAYU SANTERO</h1>' +
            '<p style="text-align: center">JL. Raya Semer NO 26 Kerobokan Kuta-Badung</p>' +
            '<h3 style="text-align: center">Laporan Daftar Tamu</h3>' +
            '</div>' +
            '<br>' +

            divToPrint.innerHTML +
            '</body>' +
            '</html>');
        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

    }
</script>