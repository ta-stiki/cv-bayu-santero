<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:18 AM
 */
$data_tamu = new stdClass();
$nama_tamu = '';
$jenis_kelamin = '';
$kewarganegaraan = '';
$proses = '';
$no_hp = '';
$id_agen = '';
$jumlah_pembayaran = '';
$status_pembayaran = '';
$jumlah_dp = 0;
?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-title text-center">Tambah Data Tamu</h6>
    <?php include_once __DIR__ . '/_data-tamu-form.php';?>
</div>
