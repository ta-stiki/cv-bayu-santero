<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:09 PM
 */

$monthyear = isset($_POST['monthyear'])? $_POST['monthyear']: date('m-Y');

?>
<div id="chartContainer" class="card pd-20 pd-sm-40">
    <h6 class="card-body-title tx-center">Grafik Keuangan Perusahaan</h6>
    <h3 id="month" class="show-print card-body-title tx-center"></h3>
    <div class="no-print row pd pd-b-5">
        <form id="search" action="" method="post" style="width: 100%">
            <div class="row">
                <div class="col-md-6">
                    <div class="row tx-right">
                        <div class="col-lg-2 mg-t-20 mg-l-20 mg-lg-t-0">
                            <label for="">Periode</label>
                        </div>
                        <div class="col-lg-8 mg-t-20 mg-lg-t-0">
                            <div class="input-group">
                                <select name="monthyear" class="form-control" id="">
                                    <?php
                                    for ($i=12; $i >=0; $i--){
                                        $date = date("m-Y", strtotime("-{$i} month", time()));
                                        $dateVal = date("F-Y", strtotime("-{$i} month", time()));
                                        $selected = ($monthyear == $date)? 'selected':null;
                                        echo "<option $selected value=\"{$date}\">{$dateVal}</option>";
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="tx-right">
                            <button onclick="window.print()" class="btn btn-outline-primary mg-b-10"><i class="fa fa-print"></i> Cetak</button>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
    <canvas id="chartBar2" height="300"></canvas>
</div>
<script>

    Number.prototype.formatMoney = function(c, d, t){
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
    var chartData = {};
    $(document).ready(function () {

        // Define a plugin to provide data labels
        Chart.plugins.register({
            afterDatasetsDraw: function(chart, easing) {
                // To only draw at the end of animation, check for easing === 1
                var ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function(element, index) {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgb(0, 0, 0)';

                            var fontSize = 16;
                            var fontStyle = 'normal';
                            var fontFamily = 'Helvetica Neue';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            var dataString = (dataset.data[index]).formatMoney(2,',','.').toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            var padding = 5;
                            var position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                        });
                    }
                });
            }
        });

        function respondCanvas() {
            $('#chartBar2').remove();
            $('#chartContainer').append("<canvas id=\"chartBar2\" height=\"300\"></canvas>");

            var ctx2,myChart2 = null;
            ctx2 = document.getElementById('chartBar2').getContext('2d');
            myChart2 = new Chart(ctx2, {
                type: 'bar',
                data: chartData,
//                data: {
//                    labels: ['Pemasukan', 'Pengeluaran', 'Keuntungan'],
//                    datasets: [{
//                        label: 'Total Transaksi',
//                        data: [parseFloat(1500000), parseFloat(250000), parseFloat(1250000)],
//                        backgroundColor: [
//                            '#3dd300',
//                            '#f49200',
//                            '#fff800',
//                        ]
//                    }]
//                },
                options: {
                    legend: {
                        display: false,
                        labels: {
                            display: false
                        }
                    },
                    tooltips: {
                        mode: 'label',
                        label: 'mylabel',
                        callbacks: {
                            label: function(tooltipItem, data) {
                                return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            }
                        }
                    },

                    scales: {
                        yAxes: [{
                            stacked: true,
//                        ticks: {
//                            beginAtZero:true,
//                            fontSize: 10,
//                            max: 2000000
//                        }
                            ticks: {
                                callback: function(label, index, labels) {
                                    return label/1000;
                                }
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Ribu'
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                beginAtZero:true,
                                fontSize: 11
                            }
                        }]
                    }
                }
            });
        }

        $("select[name='monthyear']",'#search').on('change',function () {
            var val = $(this).val();
            $('#month').html($("select[name='monthyear'] option:selected",'#search').text());
            $.get('<?= baseUrl() . 'include/actions.php?a=grafik&q='?>' + val,function (result) {
                chartData = JSON.parse(result);
                respondCanvas();
            })
        });
        $("select[name='monthyear']",'#search').change();

    })
</script>
