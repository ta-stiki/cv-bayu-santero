<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:20 AM
 */
$dataID = isset($_GET['id'])? escape($_GET['id']):null;
$data_tamu = new stdClass();
if($dataID){
    $data_tamu = showDataKewarganegaraanByID($dataID);

    $country_code = $data_tamu->country_code;
    $country_name = $data_tamu->country_name;
}
?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-title text-center">Update Data Kewarganegaraan</h6>
    <?php include_once __DIR__ . '/_data-kewarganegaraan-form.php';?>
</div>

