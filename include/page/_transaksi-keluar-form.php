<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/29/2017
 * Time: 7:30 PM
 */

//$q = isset($_POST['q'])? escape($_POST['q']): null;
//$dataTamu = showDataTransaksi($q);
//$id_tamu = '';

if(isset($_POST['aksi'])){
    $detailJournal = [];
    $aksi = escape($_POST['aksi']);
    $keterangan_transaksi = (isset($_POST['keterangan_transaksi']))? escape($_POST['keterangan_transaksi']):null;
    $no_bukti = (isset($_POST['no_bukti']))? escape($_POST['no_bukti']):null;
    $kode_akun = (isset($_POST['kode_akun']))? $_POST['kode_akun']:null;
    $debit = (isset($_POST['debit']))? $_POST['debit']:null;
    $credit = (isset($_POST['credit']))? $_POST['credit']:null;
    $tanggal = (isset($_POST['tanggal']))? $_POST['tanggal']:null;
    switch ($aksi){
        case 'transaksi-keluar-add':
            if(is_array($kode_akun)){
                $detailJournal = [];
                $jml = count($kode_akun);
                $tDebit =0;
                $tCredit =0;
                foreach ($kode_akun as $key=> $value){
                    $tDebit +=$debit[$key];
                    $tCredit +=$credit[$key];
                    $detailJournal[] = [
                        'kode_akun' => $value, //Pendapatan Usaha
                        'debit' => floatval($debit[$key]),
                        'credit' => floatval($credit[$key])
                    ];
                }

                $transaksi = insertTransaksi(null,$tDebit,null,$keterangan_transaksi,null,null,2,'Lunas','Keluar');
                if($transaksi){
                    insertJournal($transaksi,$keterangan_transaksi,$tanggal,$detailJournal);
                }
            }

            break;
        case 'transaksi-keluar-update':
            break;
    }
    ?>
    <script>
        window.location = '<?= url('index.php?page=transaksi-keluar')?>';
    </script>
    <?php
}

?>
<style>
    fieldset {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
        width:inherit;
        /*border-width:1px;*/
        /*padding:0 10px;*/
        /*border-style:groove;*/
        /*border-color:dimgray;*/
    }
    fieldset > legend {
        width: inherit;
        padding: 0px 10px 0px 10px;
        text-align: center;
    }
</style>
<div class="row pd-t-10">
    <div class="col-md-12">
        <form class="form-horizontal" method="post">
            <fieldset>
                <input type="hidden" name="aksi" value="<?= $page?>">
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="tunggakan">ID Transaksi</label>
                    <div class="col-md-4">
                        <input <?= ($isNew)? 'readonly':null?> id="id_transaksi" name="id_transaksi" type="text" placeholder="Tunggakan" class="form-control input-md"  value="<?= $id_transaksi?>">
                    </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="tunggakan">Tanggal</label>
                    <div class="col-md-4">
                        <input required id="tanggal" name="tanggal" type="text" placeholder="Tanggal" class="fc-datepicker form-control input-md"  value="<?= $tanggal?>">
                    </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="tunggakan">No Bukti</label>
                    <div class="col-md-8">
                        <input required id="no_bukti" name="no_bukti" type="text" placeholder="No Bukti" class="form-control input-md"  value="<?= $no_bukti?>">
                    </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="tunggakan">Keterangan</label>
                    <div class="col-md-8">
                        <input required id="keterangan_transaksi" name="keterangan_transaksi" type="text" placeholder="Keterangan" class="form-control input-md"  value="<?= $keterangan_transaksi?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <table class="table" id="table-transaksi">
                        <thead>
                        <tr>
                            <th>Kode Rekening</th>
                            <th>Debit</th>
                            <th>Kredit</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <select name="kode_akun[]" id="" class="form-control input-md">
                                    <?php
                                    listOptionDataakun();
                                    ?>
                                </select>
                            </td>
                            <td><input name="debit[]" type="text" class="form-control input-md"></td>
                            <td><input name="credit[]" type="text" class="form-control input-md"></td>
                            <td>
                                <button type="button" class="remove_row btn btn-sm btn-danger"><i class="fa fa-times"></i></button>
                                <button type="button" class="add_row btn btn-sm btn-info"><i class="fa fa-plus"></i></button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>


                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="button1id"></label>
                    <div class="col-md-8">
                        <button type="submit" id="button1id" name="button1id" class="btn btn-success">Proses</button>
                        <a id="button2id" name="button2id" class="btn btn-danger" href="<?= url('index.php?page=transaksi-masuk')?>">Batal</a>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        
        function addRow() {

        }
        $('.select2').select2({
//            minimumResultsForSearch: Infinity
            minimumResultsForSearch: ''

        });

        $('#table-transaksi').on('click','.add_row',function () {
//            $('.select2').select2('destroy');
            var tr = $(this).parents('tr');
            var cloneTr = $(tr).clone();
            var tbody = $(this).parents('tbody');
            $(tbody).append(cloneTr);
//            $('.select2').select2({
//                minimumResultsForSearch: ''
//            });
        });
        $('#table-transaksi').on('click','.remove_row',function () {
//            $('.select2').select2('destroy');
            var tr = $(this).parents('tr');
//            var cloneTr = $(tr).clone();
            var tbody = $(this).parents('tbody');
            var rowCount = $('tr', tbody).length;
            if(rowCount >1){
                $(tr).remove();
            }
//            $('.select2').select2({
//                minimumResultsForSearch: ''
//            });
        });


        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: "yy-mm-dd"
        });
        $('#jenis_transaksi').on('change',function () {
            var val = $(this).val();
            console.log(val);

        })
    })
</script>