<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 2/2/2018
 * Time: 8:41 PM
 */

if(isset($_GET['id'])){
    $dataTransMasuk = showTransaksiMasukByTamuID($_GET['id']);
}else{
    ?>
    <p class="text-center">Maaf akses ditolak</p>
    <div class="row pd pd-b-5">
        <a href="javascript:history.back()" class="btn btn-success mg-b-10"><i class="fa fa-backward"></i> Kembali</a>
    </div>
    <?php
    die();
}

?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">Riwayat Transaksi</h6>
    <p class="text-center"></p>
    <div class="card-body">
        <div class="row pd pd-b-5">
            <a href="javascript:history.back()" class="btn btn-success mg-b-10"><i class="fa fa-backward"></i> Kembali</a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-primary mg-b-0">
                        <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Tanggal</th>
                            <th class="text-center">ID Transaksi</th>
                            <th class="text-center">Nama Tamu</th>
                            <th class="text-center">Agen</th>
                            <th class="text-center">Jenis Transaksi</th>
                            <th class="text-center">Proses</th>
                            <th class="text-center">Debit</th>
                            <th class="text-center">Kredit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i=0;
                        $tdebit = 0;
                        $tcredit = 0;
                        if($dataTransMasuk){

                            while($item = $dataTransMasuk->fetch_object()){
                                $i++;
                                $tdebit += ($item->tipe_transaksi == 'Masuk')? $item->total_transaksi:0;
                                $tcredit += ($item->tipe_transaksi == 'Keluar')? $item->total_transaksi:0;
                                ?>
                                <tr>
                                    <td><?= $i?></td>
                                    <td><?= TanggalIndo($item->tanggal)?></td>
                                    <td><?= $item->id_transaksi?></td>
                                    <td><?= $item->nama_tamu?></td>
                                    <td><?= $item->nama_agen?></td>
                                    <td><?= jenisTransaksi($item->jenis_transaksi)?></td>
                                    <td><?= $item->proses_transaksi?></td>
                                    <td><?= angkaIndo((($item->tipe_transaksi == 'Masuk')? $item->total_transaksi:0))?></td>
                                    <td><?= angkaIndo((($item->tipe_transaksi == 'Keluar')? $item->total_transaksi:0))?></td>
                                </tr>
                                <?php
                            }
                        }else{
                            echo "<tr><td colspan='10'>Data tidak ditemukan</td></tr>";
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7" class="text-center">Total</td>
                            <td><?= angkaIndo($tdebit)?></td>
                            <td><?= angkaIndo($tcredit)?></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: "yy-mm-dd"
        });
    })
</script>


