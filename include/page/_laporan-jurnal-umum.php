<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:08 PM
 */

$dari = isset($_POST['dari'])?  escape($_POST['dari']): date('d/m/Y');
$sampai = isset($_POST['sampai'])? escape($_POST['sampai']): date('d/m/Y');
$q = isset($_POST['q'])? escape($_POST['q']): null;

//if($_POST){
//    list($dd1,$mm1,$yy1) = explode('/',$dari);
//    if(!checkdate($mm1,$dd1,$yy1)){
//        $dari = null;
//    }else{
//        $dari = dmyToYmd($dari, '/');
//    }
//
//    list($dd2,$mm2,$yy2) = explode('/',$sampai);
//    if(!checkdate($mm2,$dd2,$yy2)){
//        $sampai = null;
//    }else{
//        $sampai = dmyToYmd($sampai, '/');
//    }
//}

$jurnal = showJurnalUmum(dmyToYmd($dari,'/'), dmyToYmd($sampai,'/'), $q);

?>
<style>
.table > tbody > tr > td {
     vertical-align: middle;
}
</style>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">Laporan Jurnal Umum</h6>
    <div class="row pd pd-b-5">
        <form action="" method="post" style="width: 100%">
            <div class="row">
                <div class="col-md-6">

                    <div class="row">
                        <div class="col-lg-2 mg-t-20 mg-lg-t-0">
                            <label for="">Periode</label>
                        </div>
                        <div class="col-lg-8 mg-t-20 mg-lg-t-0">
                            <div class="input-group">
                                <input name="dari" type="text" class="form-control fc-datepicker" placeholder="dd/mm/YYYY" value="<?= $dari?>">
                                <span class="input-group-addon">S/d</span>
                                <input name="sampai" type="text" class="form-control fc-datepicker" placeholder="dd/mm/YYYY" value="<?= $sampai?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <input name="q" type="text" class="form-control" placeholder="Cari" value="<?= $q?>">
                        <span class="input-group-btn">
                    <button class="btn bd bg-white tx-gray-600"><i class="icon ion-search"></i></button>
                </span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php
    if($jurnal){
        ?>
        <button onclick="printDiv();" class="btn btn-dark"><i class="fa fa-print"></i> Cetak </button>
        <?php
    }
    ?>
    <div id="DivIdToPrint" class="table-responsive">   
        <?php
        $rows = array();
        if($jurnal){
            while ($row = $jurnal->fetch_object()){
                if (empty($rows[$row->tanggal]))
                    $rows[$row->tanggal] = array();
                $rows[$row->tanggal][$row->id_transaksi][] = $row;
            }
        }

        ?>
        <table id="tableJu" class="table table-hover table-bordered table-primary mg-b-0">
            <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">ID Transaksi</th>
                <th class="text-center">Nama Tamu</th>
                <th class="text-center">Nama Agen</th>
                <th class="text-center">Kode Akun</th>
                <th class="text-center">Nama Akun</th>
                <th class="text-center">Keterangan Transaksi</th>
                <th class="text-center">Debet</th>
                <th class="text-center">Kredit</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $tdebit = 0;
            $tcredit = 0;
            if($jurnal){
                $i = 0;
                $jurnal->data_seek(0);                
                $rowData = '';                
                foreach($rows as $tgl => $trans){                    
                    foreach($trans as $key => $itemtrans){
                        $keyCount = count($itemtrans);
                        $i++;
                        foreach($itemtrans as $data){    
                            $tdebit += $data->debit;
                            $tcredit += $data->credit;
                            $rowColor = ($data->klasifikasi == 'F')? '#FFCC33':null;
                            $rowData .="<tr style=\"background-color: $rowColor;\">";
                            if($keyCount){
                                $rowData .= "<td rowspan ='".count($itemtrans)."' >". $i. "</td>";
                                $rowData .= "<td rowspan ='".count($itemtrans)."' >". ymdToDmy($tgl). "</td>";
                                $rowData .= "<td rowspan ='".count($itemtrans)."' >". $key. "</td>";
                                $rowData .= "<td rowspan ='".count($itemtrans)."' >". $data->nama_tamu. "</td>";
                                $rowData .= "<td rowspan ='".count($itemtrans)."' >". $data->nama_agen. "</td>";
                                $keyCount =0;
                            }else{                                
                                //$rowData .= "<td rowspan ='".count($itemtrans)."' >". $key. "</td>";
                            }
                            
                            $rowData .= "                            
                            <td>". $data->kode_rekening. "</td>
                            <td>". $data->nama_rekening. "</td>
                            <td>". $data->keterangan_transaksi. "</td>
                            <td class=\"text-right\">". angkaIndo($data->debit). "</td>
                            <td class=\"text-right\">". angkaIndo($data->credit). "</td>
                            ";
                            $rowData .="</tr>";
                        }
                    }
                }
                print_r($rowData);
            }else{
                ?>
                <tr>
                    <td colspan="10">Tidak ada data yang ditampilkan</td>
                </tr>
                <?php
            }
            ?>                   
            </tbody>
            <tfoot>
            <tr>
                <td colspan="8" class="text-center">Total Transaksi</td>
                <td class="text-right"><?= angkaIndo($tdebit)?></td>
                <td class="text-right"><?= angkaIndo($tcredit)?></td>
            </tr>
            </tfoot>
        </table>
    </div><!-- table-responsive -->
</div>
<script>
    $(document).ready(function () {
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: "dd/mm/yy"
        });
        
        //groupTable($('#tableJu tr:has(td)'),0,3);
        //$('#tableJu .deleted').remove();
    })

    function printDiv()
    {

        var divToPrint=document.getElementById('DivIdToPrint');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html>' +
            '<link href="<?= url('assets/css/print.css')?>" rel="stylesheet">' +
            '<body onload="window.print()">'+
            '<h1 style="text-align: center">CV BAYU SANTERO</h1>' +
            '<h4 style="text-align: center">JL. Raya Semer NO 26 Kerobokan Kuta-Badung</h3>' +
            '<h3 style="text-align: center">LAPORAN JURNAL UMUM</h3>' +
            '<br>' +

            divToPrint.innerHTML +
            '</body>' +
            '</html>');
        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

    }
</script>