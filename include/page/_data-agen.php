<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:07 PM
 */

$q = isset($_POST['q'])? escape($_POST['q']): null;
$dataTamu = showDataAgen($q);

?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">DATA AGEN</h6>
    <div class="row pd pd-b-5">
        <div class="col-md-6">
            <a href="<?= url('index.php?page=data-agen-add')?>" class="btn btn-success mg-b-10"><i class="fa fa-plus-circle"></i> Tambah Data Agen</a>
        </div>
        <div class="col-md-6">
            <form action="" method="post">
                <div class="input-group">
                    <input name="q" type="text" class="form-control" placeholder="Cari" value="<?= $q?>">
                    <span class="input-group-btn">
                    <button class="btn bd bg-white tx-gray-600"><i class="icon ion-search"></i></button>
                </span>
                </div>
            </form>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered table-primary mg-b-0">
            <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">ID Agen</th>
                <th class="text-center">Nama Agen</th>
                <th class="text-center">alamat Agen</th>
                <th class="text-center">No Telp</th>
                <th class="text-center">Keterangan</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($dataTamu){
                $i=0;
                /**
                 * @var $dataTamu mysqli_result
                 **/
                while($item = $dataTamu->fetch_object()){
                    $i++;
                    ?>
                    <tr>
                        <td><?= $i?></td>
                        <td><?= $item->id_agen?></td>
                        <td><?= $item->nama_agen?></td>
                        <td><?= $item->alamat_agen?></td>
                        <td><?= $item->no_telp?></td>
                        <td>
                            <?php
                    if($identity->akses == 'admin'){
                        ?>
                        <a href="<?= url('index.php?page=data-agen-update&id='. $item->id_agen)?>" class="btn btn-outline-primary btn-icon mg-r-5">
                            <div>
                                <i class="fa fa-edit"></i>
                            </div>
                        </a>
                        <?php
                    }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }else{
                echo "<tr><td colspan='6'>Data tidak ditemukan</td></tr>";
            }
            ?>
            </tbody>
        </table>
    </div><!-- table-responsive -->
</div>
