<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:09 PM
 */
?>
<div class="card pd-20 pd-sm-40">
    <h6 class="card-body-title">Grafik Keuangan Perusahaan</h6>
    <div class="no-print row pd pd-b-5">
        <form action="" method="post" style="width: 100%">
            <div class="row">
                <div class="col-md-6">

                    <div class="row tx-right">
                        <div class="col-lg-2 mg-t-20 mg-l-20 mg-lg-t-0">
                            <label for="">Periode</label>
                        </div>
                        <div class="col-lg-8 mg-t-20 mg-lg-t-0">
                            <div class="input-group">
                                <select name="" class="form-control" id="">
                                    <option value="Jan-2017">Januari 2017</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <canvas id="chartBar2" height="300"></canvas>
</div>
<script>
    $(document).ready(function () {
        // Define a plugin to provide data labels
        Chart.plugins.register({
            afterDatasetsDraw: function(chart, easing) {
                // To only draw at the end of animation, check for easing === 1
                var ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function(element, index) {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgb(0, 0, 0)';

                            var fontSize = 16;
                            var fontStyle = 'normal';
                            var fontFamily = 'Helvetica Neue';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            var dataString = dataset.data[index].toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            var padding = 5;
                            var position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                        });
                    }
                });
            }
        });
        var ctx2 = document.getElementById('chartBar2').getContext('2d');
        var myChart2 = new Chart(ctx2, {
            type: 'bar',
            data: {
                labels: ['Pemasukan', 'Pengeluaran', 'Keuntungan'],
                datasets: [{
                    label: 'Total Transaksi',
                    data: [parseFloat(1500000), parseFloat(250000), parseFloat(1250000)],
                    backgroundColor: [
                        '#3dd300',
                        '#f49200',
                        '#fff800',
                    ]
                }]
            },
            options: {
                legend: {
                    display: false,
                    labels: {
                        display: false
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            fontSize: 10,
                            max: 2000000
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero:true,
                            fontSize: 11
                        }
                    }]
                }
            }
        });

    })
</script>
