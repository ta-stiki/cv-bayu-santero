<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:18 AM
 */
$id = (isset($data_tamu->id))?$data_tamu->id:'';

if(isset($_POST['aksi'])){
    $aksi = escape($_POST['aksi']);
    $country_code = escape($_POST['country_code']);
    $country_name = escape($_POST['country_name']);

    switch ($aksi){
        case 'data-kewarganegaraan-update':
            $queryText = "UPDATE apps_countries
            SET
            country_code = '$country_code',
            country_name = '$country_name'  
            WHERE
            id = '$id'
            ";
            query($queryText);
            break;
        case 'data-kewarganegaraan-add':
            query("INSERT INTO apps_countries VALUES ('$country_code','$country_name')");
            break;

    }
    ?>
    <script>
        window.location = '<?= url('index.php?page=data-kewarganegaraan')?>';
    </script>
    <?php
}
?>
<style>
    fieldset {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
        width:inherit;
        /*border-width:1px;*/
        /*padding:0 10px;*/
        /*border-style:groove;*/
        /*border-color:dimgray;*/
    }
    fieldset > legend {
        width: inherit;
        padding: 0px 10px 0px 10px;
        text-align: center;
    }
</style>

<div class="row">
    <div class="col-md-12">

        <form class="form-horizontal" method="post">
            <input type="hidden" name="aksi" value="<?= $page?>">
            <fieldset>
                <div class="row">
                    <div class="col-md-12">
                        <p style="float: right;padding:20px">Tanggal: <?= date('d-m-Y')?></p>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="country_code">Kode</label>
                    <div class="col-md-8">
                        <input id="country_code" name="country_code" type="text" placeholder="Kode Negara" class="form-control input-md" required="" value="<?= $country_code?>">

                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="country_name">Kewarganegaraan</label>
                    <div class="col-md-8">
                        <textarea class="form-control" id="country_name" name="country_name" placeholder="Kewarganegaraan"><?= $country_name?></textarea>
                    </div>
                </div>

                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="actionsave"></label>
                    <div class="col-md-8">
                        <button id="actionsave" name="actionsave" class="btn btn-success">Save</button>
                        <a id="button2id" name="button2id" class="btn btn-danger" href="<?= url('index.php?page=data-kewarganegaraan')?>">Cancel</a>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>