<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:18 AM
 */
$id = (isset($data_tamu->id))?$data_tamu->id:autoNumber('id_agen','AG','agen');

if(isset($_POST['aksi'])){
    $aksi = escape($_POST['aksi']);
    $proses = escape($_POST['proses']);
    $harga = escape($_POST['harga']);

    switch ($aksi){
        case 'data-proses-update':
            $queryText = "UPDATE master_proses_visa
            SET
            proses = '$proses',
            harga = '$harga'                        
            WHERE
            id = '$id'
            ";
            query($queryText);
            break;
        case 'data-proses-add':
            query("INSERT INTO master_proses_visa (proses, harga) VALUES ('$proses','$harga')");
            break;

    }
    ?>
    <script>
        window.location = '<?= url('index.php?page=data-proses')?>';
    </script>
    <?php
}
?>
<style>
    fieldset {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
        width:inherit;
        /*border-width:1px;*/
        /*padding:0 10px;*/
        /*border-style:groove;*/
        /*border-color:dimgray;*/
    }
    fieldset > legend {
        width: inherit;
        padding: 0px 10px 0px 10px;
        text-align: center;
    }
</style>

<div class="row">
    <div class="col-md-12">

        <form class="form-horizontal" method="post">
            <input type="hidden" name="aksi" value="<?= $page?>">
            <input type="hidden" name="id" value="<?= $id?>">
            <fieldset>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nama_agen">Nama Jasa</label>
                    <div class="col-md-8">
                        <input id="proses" name="proses" type="text" placeholder="Jasa" class="form-control input-md" required="" value="<?= $proses?>">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="no_telp">Harga</label>
                    <div class="col-md-8">
                        <input required id="harga" name="harga" type="text" placeholder="Harga" class="form-control input-md" value="<?= $harga?>">

                    </div>
                </div>

                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="actionsave"></label>
                    <div class="col-md-8">
                        <button id="actionsave" name="actionsave" class="btn btn-success">Save</button>
                        <a id="button2id" name="button2id" class="btn btn-danger" href="<?= url('index.php?page=data-proses')?>">Cancel</a>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>