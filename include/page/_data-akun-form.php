<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:18 AM
 */
$id = (isset($data_tamu->kode_rekening))?$data_tamu->kode_rekening:null;

if(isset($_POST['aksi'])){
    $aksi = escape($_POST['aksi']);
    $nama_rekening = escape($_POST['nama_rekening']);
    $klasifikasi = escape($_POST['klasifikasi']);

    switch ($aksi){
        case 'data-akun-update':
            $queryText = "UPDATE akun
            SET
            nama_rekening = '$nama_rekening',
            klasifikasi = '$klasifikasi'                        
            WHERE
            id_agen = '$id'
            ";
            query($queryText);
            break;
        case 'data-akun-add':
            query("INSERT INTO akun VALUES ('$id','$nama_rekening','$klasifikasi')");
            break;

    }
    ?>
    <script>
        window.location = '<?= url('index.php?page=data-akun')?>';
    </script>
    <?php
}
?>
<style>
    fieldset {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
        width:inherit;
        /*border-width:1px;*/
        /*padding:0 10px;*/
        /*border-style:groove;*/
        /*border-color:dimgray;*/
    }
    fieldset > legend {
        width: inherit;
        padding: 0px 10px 0px 10px;
        text-align: center;
    }
</style>

<div class="row">
    <div class="col-md-12">

        <form class="form-horizontal" method="post">
            <input type="hidden" name="aksi" value="<?= $page?>">
            <fieldset>
                <div class="row">
                    <div class="col-md-12">
                        <p style="float: right;padding:20px">Tanggal: <?= date('d-m-Y')?></p>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="id_agen">Kode Rekening</label>
                    <div class="col-md-8">
                        <input  id="kode_rekening" name="kode_rekening" type="text" placeholder="Kode Rekening" class="form-control input-md" required="" value="<?= $id?>">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nama_agen">Nama Rekening</label>
                    <div class="col-md-8">
                        <input id="nama_rekening" name="nama_rekening" type="text" placeholder="Nama Rekening" class="form-control input-md" required="" value="<?= $nama_rekening?>">

                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="proses">Tipe Akun</label>
                    <div class="col-md-8">
                        <?php
                        $visa = showDataAkunKlasifikasi();
                        ?>
                        <select required id="klasifikasi" name="klasifikasi" class="form-control">
                            <option value="">--Pilih Tipe Akun--</option>
                            <?php
                            while ($rowVisa = $visa->fetch_object()){
                                $select = ($klasifikasi == $rowVisa->klasifikasi)? 'selected':null;
                                echo "<option value=\"{$rowVisa->klasifikasi}\" $select>{$rowVisa->deskripsi} | {$rowVisa->normal}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="proses">Saldo Normal</label>
                    <div class="col-md-8">
                        <select required id="saldo_normal" name="saldo_normal" class="form-control">
                            <option value="Debit">Debit</option>
                            <option value="Kredit">Kredit</option>
                        </select>
                    </div>
                </div>

                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="actionsave"></label>
                    <div class="col-md-8">
                        <button id="actionsave" name="actionsave" class="btn btn-success">Save</button>
                        <a id="button2id" name="button2id" class="btn btn-danger" href="<?= url('index.php?page=data-akun')?>">Cancel</a>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>