<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/29/2017
 * Time: 7:30 PM
 */

//$q = isset($_POST['q'])? escape($_POST['q']): null;
//$dataTamu = showDataTransaksi($q);
//$id_tamu = '';

//if($_POST){
//    echo '<pre>';
//    print_r($_POST);
//    echo '</pre>';
//
//}
if(isset($_POST['aksi'])){
    $detailJournal = [];
    $aksi = escape($_POST['aksi']);
    $jenis_transaksi = (isset($_POST['jenis_transaksi']))? escape($_POST['jenis_transaksi']):null;
    $tanggal = (isset($_POST['tanggal']))? escape($_POST['tanggal']):date('Y-m-d');
    $nama_tamu = (isset($_POST['nama_tamu']))? escape($_POST['nama_tamu']):null;
    $proses = (isset($_POST['proses']))? escape($_POST['proses']):null;
    $tunggakan = (isset($_POST['tunggakan']))? escape($_POST['tunggakan']):null;
    $bayar = (isset($_POST['bayar']))? escape($_POST['bayar']):null;
    $sisa_tunggakan = (isset($_POST['sisa_tunggakan']))? escape($_POST['sisa_tunggakan']):null;
    $no_bukti = (isset($_POST['no_bukti']))? escape($_POST['no_bukti']):null;

    $dataTamu = showDataTamuByID($nama_tamu);
    $namaTamu = (isset($dataTamu->nama_tamu))? $dataTamu->nama_tamu:null;
    $induk=null;
    $keterangan_transaksi = '';
    $total = 0;
    switch ($aksi){
        case 'transaksi-masuk-add':
            if($sisa_tunggakan){
                $statusTunggakan = 'Belum Lunas';
            }else{
                $statusTunggakan = 'Lunas';
            }

            if($jenis_transaksi == 1){ // Perpanjangan Visa


                $keterangan_transaksi = 'Perpanjangan Visa ' . $namaTamu .' dengan proses ' . getNameProsesVisa($proses);
                $induk = null;
                $total = $tunggakan;

                if($sisa_tunggakan > 0){
                    $detailJournal = [
                        [
                            'kode_akun' => '4.1', //Pendapatan Usaha
                            'debit' => 0,
                            'credit' => $tunggakan
                        ],
                        [
                            'kode_akun' => '1.2', //Piutang Usaha
                            'debit' => $sisa_tunggakan,
                            'credit' => 0
                        ],
                        [
                            'kode_akun' => '1.1', //Kas
                            'debit' => $bayar,
                            'credit' => 0
                        ],
                    ];
                }else{
                    $detailJournal = [
                        [
                            'kode_akun' => '4.1', //Pendapatan Usaha
                            'debit' => 0,
                            'credit' => $bayar
                        ],
                        [
                            'kode_akun' => '1.1', //Kas
                            'debit' => $tunggakan,
                            'credit' => 0
                        ],
                    ];
                }

            }elseif($jenis_transaksi == 2){ // Pelunasan Visa

                $keterangan_transaksi = 'Pelunasan Visa' . $namaTamu . ' dengan proses ' . getNameProsesVisa($proses);
                $induk = getTransaksiIDByIDProsesAndIDTamu($proses,$nama_tamu);
                updateTransaksiTunggakanByID($induk,$sisa_tunggakan,$statusTunggakan);
                $sisa_tunggakan = 0;
                $total = $bayar;
                $statusTunggakan = 'Lunas';

                $detailJournal = [
                    [
                        'kode_akun' => '1.2', //Piutang Usaha
                        'debit' => 0,
                        'credit' => $bayar
                    ],
                    [
                        'kode_akun' => '1.1', //Kas
                        'debit' => $bayar,
                        'credit' => 0
                    ],

                ];
            }



            $transaksi = insertTransaksi($nama_tamu,$total,$sisa_tunggakan,$keterangan_transaksi,$proses,$induk,$jenis_transaksi,$statusTunggakan,'Masuk');
            if($transaksi){
                insertJournal($transaksi,$keterangan_transaksi,$tanggal,$detailJournal);
                ?>
                <script>
                    var win = window.open('<?= url("invoice.php?id={$transaksi}")?>','_blank');
                    win.print();
                </script>
                <?php
            }


            break;
        case 'transaksi-masuk-update':
            break;
    }

    ?>
    <script>
        window.location = '<?= url('index.php?page=transaksi-masuk')?>';
    </script>
    <?php
}

?>
<style>
    fieldset {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
        width:inherit;
        /*border-width:1px;*/
        /*padding:0 10px;*/
        /*border-style:groove;*/
        /*border-color:dimgray;*/
    }
    fieldset > legend {
        width: inherit;
        padding: 0px 10px 0px 10px;
        text-align: center;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="post">
            <input type="hidden" name="aksi" value="<?= $page?>">
            <fieldset>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 control-label" for="tunggakan">ID Transaksi</label>
                        <div class="col-md-3">
                            <input required <?= ($isNew)? 'readonly':null?> id="id_transaksi" name="id_transaksi" type="text" placeholder="Tunggakan" class="form-control input-md"  value="<?= $id_transaksi?>">
                        </div>
                        <label class="col-md-2 control-label" for="tunggakan">No Invoice</label>
                        <div class="col-md-3">
                            <input required id="no_bukti" name="no_bukti" type="text" placeholder="No Invoice" class="form-control input-md"  value="<?= $no_bukti?>">
                        </div>
                    </div>

                </div>
                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <input required name="tanggal" type="text" class="form-control fc-datepicker" placeholder="dd/mm/YYYY" value="<?= $tanggal?>">
                    </div>
                </div>
                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="status_pembayaran">Status Pembayaran</label>
                    <div class="col-md-8">
                        <label class="radio-inline" for="status_pembayaran-0">
                            <input class="jenis_transaksi" type="radio" name="jenis_transaksi" id="status_pembayaran-0" value="1" checked="checked">
                            Jasa Baru
                        </label>
                        <label class="radio-inline" for="status_pembayaran-1">
                            <input class="jenis_transaksi" type="radio" name="jenis_transaksi" id="status_pembayaran-1" value="2">
                            Pelunasan
                        </label>
                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nama_tamu">Nama Tamu</label>
                    <div class="col-md-8">
                        <select required id="nama_tamu" name="nama_tamu" class="form-control select2">
                            <option value="">--Pilih Tamu--</option>
                            <?php
                            /**
                             * @var $agen mysqli_result
                             **/
                            $agen = showDataTamu();
                            if($agen){
                                while ($rowAgen = $agen->fetch_object()){
                                    $select = ($id_tamu == $rowAgen->id_tamu)? 'selected':null;
                                    echo "<option value=\"{$rowAgen->id_tamu}\" $select>{$rowAgen->nama_tamu}</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="proses">Proses</label>
                    <div class="col-md-8">
                        <select required id="proses" name="proses" class="form-control select2">
                            <option value="">--Pilih Proses--</option>
                            <?php
                            /**
                             * @var $proses mysqli_result
                             **/
                            $proses = showMasterProsesVisa();
                            while ($rowProses = $proses->fetch_object()){
                                $select = ($id_tamu == $rowProses->id)? 'selected':null;
                                echo "<option value=\"{$rowProses->id}\" $select>{$rowProses->proses}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="tunggakan">Tunggakan</label>
                    <div class="col-md-8">
                        <input required id="tunggakan" name="tunggakan" type="text" placeholder="Tunggakan" class="form-control input-md numberinput">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="bayar">Bayar</label>
                    <div class="col-md-8">
                        <input required id="bayar" name="bayar" type="text" placeholder="Bayar" class="form-control input-md numberinput">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="sisa_tunggakan">Sisa Tunggakan</label>
                    <div class="col-md-8">
                        <input required id="sisa_tunggakan" name="sisa_tunggakan" type="text" placeholder="Sisa Tunggakan" class="form-control input-md numberinput">

                    </div>
                </div>

                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="button1id"></label>
                    <div class="col-md-8">
                        <button type="submit" id="btnBayar" name="button1id" class="btn btn-success">Proses</button>
                        <a id="button2id" name="button2id" class="btn btn-danger" href="<?= url('index.php?page=transaksi-masuk')?>">Batal</a>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#btnBayar').prop('disabled', true);
        $('#nama_tamu').select2({
//            minimumResultsForSearch: Infinity
            minimumResultsForSearch: ''
        });

        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: "yy-mm-dd"
        });

        <?php if($page == 'transaksi-masuk-add'){
            ?>
        $('.jenis_transaksi').on('change',function () {
            var val = $(this).val();
            console.log(val);
            switch (val){
                case '1' : //perpanjang visa
                    $.get('<?= baseUrl() . 'include/actions.php?a=list_tamu'?>',function (result) {
                        $('#nama_tamu').select2('destroy');
                        $('#nama_tamu').html(result);
                        $('#nama_tamu').select2({
                            minimumResultsForSearch: ''
                        });
                    });
                    break;
                case '2' ://pelunasan visa
                    $.get('<?= baseUrl() . 'include/actions.php?a=list_tamu_tunggakan'?>',function (result) {
                        $('#nama_tamu').select2('destroy');
                        $('#nama_tamu').html(result);
                        $('#nama_tamu').select2({
                            minimumResultsForSearch: ''
                        });
                    });
                    break;

            }

        });

        $('#nama_tamu').on('change',function () {
            var jenis_transaksi = $('input[name=jenis_transaksi]:checked').val();
            var val = $(this).val();
            switch (jenis_transaksi){
                case '1' : //perpanjang visa
                    $.get('<?= baseUrl() . 'include/actions.php?a=list_prose'?>',function (result) {
                        $('#proses').html(result);
                    });
                    break;
                case '2' ://pelunasan visa
                    $.get('<?= baseUrl() . 'include/actions.php?a=list_proses_tunggakan&id='?>' + val,function (result) {
                        $('#proses').html(result);
                    });
                    break;
            }


        });
        $('#proses').on('change',function () {
            var jenis_transaksi = $('input[name=jenis_transaksi]:checked').val();
            var id_tamu = $('#nama_tamu').val();
            var proses = $(this).val();

            switch (jenis_transaksi){
                case '1' : //perpanjang visa
                    $.get('<?= baseUrl() . 'include/actions.php?a=tarif_proses&id='?>' + proses,function (result) {

                        $('#tunggakan').val(result);
                        var jumlah_pembayaran =  $('#tunggakan').inputmask('unmaskedvalue');
                        var jumlah_dp =  $('#bayar').inputmask('unmaskedvalue');
                        var sisa_pembayaran = jumlah_pembayaran - jumlah_dp;
                        $('#sisa_tunggakan').val(sisa_pembayaran);
                    });
                    break;
                case '2' ://pelunasan visa

                    $.get('<?= baseUrl() . 'include/actions.php?a=jumlah_tunggakan&id='?>' + id_tamu + '&id2=' + proses,function (result) {
                        $('#tunggakan').val(result);
                        var jumlah_pembayaran =  $('#tunggakan').inputmask('unmaskedvalue');
                        var jumlah_dp =  $('#bayar').inputmask('unmaskedvalue');
                        var sisa_pembayaran = jumlah_pembayaran - jumlah_dp;
                        $('#sisa_tunggakan').val(sisa_pembayaran);
                    });
                    break;
            }


        });

        $('#bayar').on('blur',function () {
            var jumlah_pembayaran =  $('#tunggakan').inputmask('unmaskedvalue');
            var jumlah_dp =  $('#bayar').inputmask('unmaskedvalue');
            var sisa_pembayaran = jumlah_pembayaran - jumlah_dp;
            if(sisa_pembayaran < 0) {
                alert("Maaf jumlah pembayaran melebihi Tunggakan");
                $('#btnBayar').prop('disabled', true);
            }else{
                $('#btnBayar').prop('disabled', false);
            }
            $('#sisa_tunggakan').val(sisa_pembayaran);
        });


        <?php
    }?>

    })
</script>