<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:06 PM
 */

$q = isset($_POST['q'])? escape($_POST['q']): null;
$dataTamu = showDataTamu($q);

?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">DATA TAMU</h6>
    <div class="row pd pd-b-5">
        <div class="col-md-6">
            <a href="<?= url('index.php?page=data-tamu-add')?>" class="btn btn-success mg-b-10"><i class="fa fa-plus-circle"></i> Tambah Data Tamu</a>
        </div>
        <div class="col-md-6">
            <form action="" method="post">
                <div class="input-group">
                    <input name="q" type="text" class="form-control" placeholder="Cari" value="<?= $q?>">
                    <span class="input-group-btn">
                    <button class="btn bd bg-white tx-gray-600"><i class="icon ion-search"></i></button>
                </span>
                </div>
            </form>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered table-primary mg-b-0">
            <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">ID Tamu</th>
                <th class="text-center">Nama Tamu</th>
                <th class="text-center">Jenis Kelamin</th>
                <th class="text-center">Kewarganegaraan</th>
                <th class="text-center">Proses</th>
                <th class="text-center">No HP</th>
                <th class="text-center">Agen</th>
                <th class="text-center">Keterangan</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($dataTamu){
                $i=0;
                /**
                 * @var $dataTamu mysqli_result
                **/
                while($item = $dataTamu->fetch_object()){
                    $i++;
                    ?>
                    <tr>
                        <td><?= $i?></td>
                        <td><?= $item->id_tamu?></td>
                        <td><?= $item->nama_tamu?></td>
                        <td><?= $item->jenis_kelamin?></td>
                        <td><?= $item->kewarganegaraan?></td>
                        <td><?= $item->proses_visa?></td>
                        <td><?= $item->no_hp?></td>
                        <td><?= $item->nama_agen?></td>
                        <td width="120px">
                            <?php
                    if($identity->akses == 'admin'){
                        ?>
                        <a href="<?= url('index.php?page=data-tamu-update&id='. $item->id_tamu)?>" class="btn btn-outline-primary btn-icon mg-r-5">
                            <div>
                                <i class="fa fa-edit"></i>
                            </div>
                        </a>
                        <a href="<?= url('index.php?page=data-tamu-history&id='. $item->id_tamu)?>" class="btn btn-outline-primary btn-icon mg-r-5">
                            <div>
                                <i class="fa fa-history"></i>
                            </div>
                        </a>
                        <?php
                    }
                            ?>

                        </td>
                    </tr>
                    <?php
                }
            }else{
                echo "<tr><td colspan='9'>Data tidak ditemukan</td></tr>";
            }
            ?>
            </tbody>
        </table>
    </div><!-- table-responsive -->
</div>
