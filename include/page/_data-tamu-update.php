<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:20 AM
 */
$dataID = isset($_GET['id'])? escape($_GET['id']):null;
$data_tamu = new stdClass();
if($dataID){
    $data_tamu = showDataTamuByID($dataID);

    $nama_tamu = $data_tamu->nama_tamu;
    $jenis_kelamin = $data_tamu->jenis_kelamin;
    $kewarganegaraan = $data_tamu->kewarganegaraan;
    $proses = $data_tamu->proses;
    $no_hp = $data_tamu->no_hp;
    $id_agen = $data_tamu->id_agen;
    $jumlah_pembayaran = $data_tamu->jumlah_pembayaran;
    $status_pembayaran = $data_tamu->status_pembayaran;
    $jumlah_dp = $data_tamu->jumlah_dp;
}
?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-title text-center">Update Data Tamu</h6>
    <?php include_once __DIR__ . '/_data-tamu-form.php';?>
</div>

