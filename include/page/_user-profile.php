<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:18 AM
 */

$dataUser = showDataUserByID($identity->user_id);
if(isset($_POST['aksi'])){
    $aksi = escape($_POST['aksi']);
    $nama_user = escape($_POST['nama_user']);
    $username = escape($_POST['username']);
    $password = escape($_POST['password']);

    switch ($aksi){
        case 'user-profile-update':
            $queryText = "UPDATE agen
            SET
            nama_agen = '$nama_agen',
            alamat_agen = '$alamat_agen',
            no_telp = '$no_telp'            
            WHERE
            id_agen = '$id'
            ";
            query($queryText);

            ?>
            <script>
                window.location = '<?= url('logout.php')?>';
            </script>
            <?php

            break;
    }
}
?>
<style>
    fieldset {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
        width:inherit;
        /*border-width:1px;*/
        /*padding:0 10px;*/
        /*border-style:groove;*/
        /*border-color:dimgray;*/
    }
    fieldset > legend {
        width: inherit;
        padding: 0px 10px 0px 10px;
        text-align: center;
    }
</style>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <div class="row">
        <div class="col-md-12">

            <form class="form-horizontal" method="post">
                <input type="hidden" name="aksi" value="user-profile-update">
                <fieldset>
                    <legend>User Profile</legend>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="id_agen">ID User</label>
                        <div class="col-md-8">
                            <input readonly id="id_user" name="id_user" type="text" placeholder="ID User" class="form-control input-md" required="" value="<?= $dataUser->id_user?>">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nama_agen">Nama User</label>
                        <div class="col-md-8">
                            <input id="nama_user" name="nama_user" type="text" placeholder="Nama User" class="form-control input-md" required="" value="<?= $dataUser->nama_user?>">

                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nama_agen">Username</label>
                        <div class="col-md-8">
                            <input id="username" name="username" type="text" placeholder="Username" class="form-control input-md" required="" value="<?= $dataUser->username?>">
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nama_agen">Password</label>
                        <div class="col-md-8">
                            <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md" required="" value="<?= $dataUser->password?>">

                        </div>
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="actionsave"></label>
                        <div class="col-md-8">
                            <button id="actionsave" name="actionsave" class="btn btn-success">Simpan</button>
                            <a id="button2id" name="button2id" class="btn btn-danger" href="<?= url('home.php')?>">Cancel</a>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
</div>
