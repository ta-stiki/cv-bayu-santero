<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:20 AM
 */
$dataID = isset($_GET['id'])? escape($_GET['id']):null;
$data_tamu = new stdClass();
if($dataID){
    $data_tamu = showDataAgenByID($dataID);

    $nama_agen = $data_tamu->nama_agen;
    $alamat_agen = $data_tamu->alamat_agen;
    $no_telp = $data_tamu->no_telp;
}
?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-title text-center">Update Transaksi Keluar</h6>
    <?php include_once __DIR__ . '/_transaksi-keluar-form.php';?>
</div>

