<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 11:08 PM
 */
$labarugi = true;
$q = isset($_POST['q'])? escape($_POST['q']): null;
$dari = isset($_POST['dari'])? escape($_POST['dari']): date('d/m/Y');
$sampai = isset($_POST['sampai'])? escape($_POST['sampai']): date('d/m/Y');

$labarugi = showLabaRugi($q, $dari, $sampai);

?>
<style>
    .table {
        border: 1px solid black;
        background-color: white;
    }
</style>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">Laporan Laba Rugi</h6>
    <div class="row pd pd-b-5">
        <form action="" method="post" style="width: 100%">
            <div class="row">
                <div class="col-md-6 pd-l-30">

                    <div class="row">
                        <div class="col-lg-2 mg-t-20 mg-lg-t-0">
                            <label for="">Periode</label>
                        </div>
                        <div class="col-lg-8 mg-t-20 mg-lg-t-0">
                            <div class="input-group">
                                <input name="dari" type="text" class="form-control fc-datepicker" placeholder="dd/mm/YYYY" value="<?= $dari?>">
                                <span class="input-group-addon">S/d</span>
                                <input name="sampai" type="text" class="form-control fc-datepicker" placeholder="dd/mm/YYYY" value="<?= $sampai?>">
                                <span class="input-group-btn">
                                    <button class="btn bd bg-white tx-gray-600"><i class="icon ion-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-right pd-r-30">
                    <button onclick="printDiv()" class="btn btn-outline-primary mg-b-10"><i class="fa fa-print"></i> Cetak</button>
                </div>
            </div>
        </form>
    </div>
    <div id="DivIdToPrint" class="table-responsive pd-t-10">
        <table class="table mg-b-0">
            <?php
            /**
             * @var $labarugi mysqli_result
             * @var $queryKlasifikasi mysqli_result
             */
            $totalDebit = 0;
            $totalKredit = 0;

            if($labarugi){
                $queryKlasifikasi =  query("SELECT * FROM akun_klasifikasi WHERE klasifikasi IN ('D','F')");

                while ($row = $queryKlasifikasi->fetch_object()){
                    $subTotalDebit = 0;
                    $subTotalKredit = 0;
                    ?>
                    <tr>
                        <td colspan="4"><b><?= $row->deskripsi?></b></td>
                    </tr>
                    <?php
                    if(is_object($labarugi)){
                        $labarugi->data_seek(0);
                        while($item = $labarugi->fetch_object()){
                            if($row->klasifikasi == $item->klasifikasi){
                                ?>
                                <tr>
                                    <td class="text-center"></td>
                                    <td colspan="2"><?= $item->nama_rekening?></td>
                                    <?php

                                    switch ($row->normal){
                                        case 'Debit':
                                            $subTotalDebit += $item->debit;
                                            ?><td class="text-right"><?= angkaIndo($item->debit)?></td><?php
                                            break;
                                        case 'Kredit':
                                            $subTotalKredit += $item->credit;
                                            ?><td class="text-right"><?= angkaIndo($item->credit)?></td><?php
                                            break;

                                    }
                                    ?>
                                </tr>
                                <?php
                            }
                        }

                        ?>
                        <tr>
                            <td colspan="3" class="text-right">Total <?= $row->deskripsi?></td>
                            <?php
                            switch ($row->normal){
                                case 'Debit':

                                    ?><td class="text-right"><?= angkaIndo($subTotalDebit)?></td><?php
                                    break;
                                case 'Kredit':

                                    ?><td class="text-right"><?= angkaIndo($subTotalKredit)?></td><?php
                                    break;
                            }

                            $totalDebit += $subTotalDebit;
                            $totalKredit += $subTotalKredit;
                            ?>
                        </tr>
                        <?php
                    }else{
                        echo "<tr>
                                    <td colspan='10' class=\"text-center\">Tentukan periode terlebihdahulu</td>
                             </tr>";
                    }

                }
            }
            $total = $totalKredit - $totalDebit;
            ?>
            <tr>
                <td colspan="3" class="text-right"><b>Laba Rugi Bersih</b></td>
                <td class="text-right"><b><?= angkaIndo($total) ?></b></td>
            </tr>
        </table>
    </div><!-- table-responsive -->
</div>
<script>
    $(document).ready(function () {
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: "dd/mm/yy"
        });
    })

    function printDiv()
    {

        var divToPrint=document.getElementById('DivIdToPrint');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html>' +
            '<link href="<?= url('assets/css/print.css')?>" rel="stylesheet">' +
            '<body onload="window.print()">'+
            '<h1 style="text-align: center">CV BAYU SANTERO</h1>' +
            '<p style="text-align: center">JL. Raya Semer NO 26 Kerobokan Kuta-Badung</p>' +
            '<h3 style="text-align: center">Laporan Laba Rugi</h3>' +
            '<p style="text-align: center"><?= "Dari ". TanggalIndo(dmyToYmd($dari,'/')) ." s/d " . TanggalIndo(dmyToYmd($sampai,'/'))?></p>' +
            '<br>' +

            divToPrint.innerHTML +
            '</body>' +
            '</html>');
        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

    }
</script>