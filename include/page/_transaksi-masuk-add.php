<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/25/2017
 * Time: 12:18 AM
 */
$data_tamu = new stdClass();
$tanggal = date('Y-m-d');
$alamat_agen = '';
$no_telp = '';
$id_transaksi = autoNumber('id_transaksi','TR','transaksi');
$isNew = true;
?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-title text-center">Tambah Transaksi Masuk</h6>
    <?php include_once __DIR__ . '/_transaksi-masuk-form.php';?>
</div>
