<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/29/2017
 * Time: 7:30 PM
 */

$q = isset($_POST['q'])? escape($_POST['q']): null;
$dari = isset($_POST['dari'])? escape($_POST['dari']):date('Y-m-d');
$sampai = isset($_POST['sampai'])? escape($_POST['sampai']): date('Y-m-d');
$dataTransMasuk = showTransaksiMasuk($q,$dari,$sampai);
?>
<div class="card pd-20 pd-sm-40 mg-t-50">
    <h6 class="card-body-title text-center">Transaksi Masuk</h6>

    <div class="card-body">
        <div class="row pd pd-b-5">
            <a href="<?= url('index.php?page=transaksi-masuk-add')?>" class="btn btn-success mg-b-10"><i class="fa fa-plus-circle"></i> Tambah Transaksi Masuk</a>
            <form action="" method="post" style="width: 100%">
                <div class="row">
                    <div class="col-md-6 pd pd-b-10">

                        <div class="row">
                            <div class="col-lg-2 mg-t-20 mg-lg-t-0">
                                <label for="">Periode</label>
                            </div>
                            <div class="col-lg-8 mg-t-20 mg-lg-t-0">
                                <div class="input-group">
                                    <input name="dari" type="text" class="form-control fc-datepicker" placeholder="dd/mm/YYYY" value="<?= $dari?>">
                                    <span class="input-group-addon">S/d</span>
                                    <input name="sampai" type="text" class="form-control fc-datepicker" placeholder="dd/mm/YYYY" value="<?= $sampai?>">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input name="q" type="text" class="form-control" placeholder="Cari" value="<?= $q?>">
                            <span class="input-group-btn">
                                <button class="btn bd bg-white tx-gray-600"><i class="icon ion-search"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-primary mg-b-0">
                        <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Tanggal</th>
                            <th class="text-center">ID Transaksi</th>
                            <th class="text-center">Nama Tamu</th>
                            <th class="text-center">Agen</th>
                            <th class="text-center">Jenis Transaksi</th>
                            <th class="text-center">Proses</th>
                            <th class="text-center">Debit</th>
                            <th class="text-center">Kredit</th>
                            <th class="text-center">Ket</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=0;
                            $tdebit = 0;
                            $tcredit = 0;
                            if($dataTransMasuk){

                                while($item = $dataTransMasuk->fetch_object()){
                                    $i++;
                                    $tdebit += ($item->tipe_transaksi == 'Masuk')? $item->total_transaksi:0;
                                    $tcredit += ($item->tipe_transaksi == 'Keluar')? $item->total_transaksi:0;
                                    ?>
                                    <tr>
                                        <td><?= $i?></td>
                                        <td><?= TanggalIndo($item->tanggal)?></td>
                                        <td><?= $item->id_transaksi?></td>
                                        <td><?= $item->nama_tamu?></td>
                                        <td><?= $item->nama_agen?></td>
                                        <td><?= jenisTransaksi($item->jenis_transaksi)?></td>
                                        <td><?= $item->proses_transaksi?></td>
                                        <td><?= angkaIndo((($item->tipe_transaksi == 'Masuk')? $item->total_transaksi:0))?></td>
                                        <td><?= angkaIndo((($item->tipe_transaksi == 'Keluar')? $item->total_transaksi:0))?></td>
                                        <td style="width: 50px">
                                            <?php
                                    if($identity->akses == 'pimpinan'){
                                        ?>
                                        <a href="<?= url('index.php?page=transaksi-masuk-update&id='. $item->id_transaksi)?>" class="btn  btn-icon mg-r-5" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <?php
                                    }
                                            ?>
                                            <a target="_blank" href="<?= url('invoice.php?id='. $item->id_transaksi)?>" class="btn  btn-icon mg-r-5 cetak-invoice" title="Cetak Invoice">
                                                <i class="icon ion-archive"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }else{
                                echo "<tr><td colspan='10'>Data tidak ditemukan</td></tr>";
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7" class="text-center">Total</td>
                                <td><?= angkaIndo($tdebit)?></td>
                                <td><?= angkaIndo($tcredit)?></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: "yy-mm-dd"
        });
    })
</script>