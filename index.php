<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/17/2017
 * Time: 9:05 PM
 */
include_once __DIR__ . '/include/koneksi.php';
start_session();
$akses = checkAksesLogin();
if (!$akses){
    header('Location:login.php');
}
$identity = getIdentity();
require_once __DIR__ . '/include/part/_header.php';
?>
<style>
    .am-mainpanel{
        margin-top: 50px;
    }
</style>
<div class="am-header">
    <div class="am-header-left">
        <a id="naviconLeft" href="" class="am-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a>
        <a id="naviconLeftMobile" href="" class="am-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
        <a href="<?= url('home.php')?>" class="am-logo">
            <img src="<?= url('assets/img/logo.png')?>" alt="" class="image-responsive" width="80px">
        </a>
    </div><!-- am-header-left -->

    <div class="am-header-right">
        <div class="dropdown dropdown-profile">
            <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                <span class="logged-name">
                    <span class="hidden-xs-down">User login: <?= $identity->nama_user?></span> <i class="fa fa-angle-down mg-l-3"></i>
                </span>
            </a>
            <div class="dropdown-menu wd-200">
                <ul class="list-unstyled user-profile-nav">
                    <li><a href="<?= url('index.php?page=user-profile')?>"><i class="icon ion-ios-person-outline"></i> Edit Profile</a></li>
                    <li><a href="<?= url('logout.php')?>"><i class="icon ion-power"></i> Sign Out</a></li>
                </ul>
            </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
    </div><!-- am-header-right -->
</div><!-- am-header -->
<?php include_once __DIR__ . '/include/part/_menu.php'?>
<div class="am-mainpanel">
    <div class="am-pagebody">
        <div class="text-center">
            <h1>CV BAYU SANTERO</h1>
            <h6>JL. Raya Semer NO 26 Kerobokan Kuta-Badung</h6>
        </div>
        <?php include_once __DIR__ .'/page.php'?>
    </div><!-- am-pagebody -->
    <?php include_once __DIR__ .'/include/part/_footer.php'?>
</div><!-- am-mainpanel -->
<!--<script src="http://maps.google.com/maps/api/js?key=AIzaSyAEt_DBLTknLexNbTVwbXyq2HSf2UbRBU8"></script>-->
<?= loadJs([
    'lib/jquery-ui/jquery-ui.js',
    'lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
    'lib/jquery-toggles/toggles.min.js',
    'lib/d3/d3.js',
    'lib/rickshaw/rickshaw.min.js',
    'lib/chart.js/Chart.js',
    'lib/gmaps/gmaps.js',
    'lib/Flot/jquery.flot.js',
    'lib/Flot/jquery.flot.pie.js',
    'lib/Flot/jquery.flot.resize.js',
    'lib/flot-spline/jquery.flot.spline.js',
    'lib/select2/js/select2.min.js',
    'js/amanda.js',
    'js/ResizeSensor.js',
//    'js/dashboard.js',
])?>
</body>
</html>

