<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 2/2/2018
 * Time: 11:32 AM
 */
include_once __DIR__ . '/include/koneksi.php';
start_session();
$akses = checkAksesLogin();
if (!$akses){
    header('Location:login.php');
}
$identity = getIdentity();
if(isset($_GET['id'])){
    $transaksi = showTransaksiMasukByID($_GET['id']);
    $rowTrans = $transaksi->fetch_object();
}else {
    echo "Maaf data tidak bisa ditampilkan. ID Treansaksi tidak sesuai";
    die();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?= loadCss([
        'lib/font-awesome/css/font-awesome.css',
        'lib/Ionicons/css/ionicons.css',
        'lib/bootstrap/css/bootstrap.min.css',
        'lib/bootstrap/css/bootstrap-grid.min.css',
        'css/print.css',
    ])?>
    <style>
        @media print {
            .container{
                width: 100%;
                min-width: 90%;
            }
        }
        .invoice-title h2, .invoice-title h3 {
            display: inline-block;
        }

        .table > tbody > tr > .no-line {
            border-top: none;
        }

        .table > thead > tr > .no-line {
            border-bottom: none;
        }

        .table > tbody > tr > .thick-line {
            border-top: 2px solid;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="invoice-title">
                <h2>Invoice</h2><h3 class="pull-right">Order # <?= $rowTrans->no_bukti?></h3>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-6">
                    <address>
                        <strong>Tagihan Kepada:</strong><br>
                        <?= $rowTrans->nama_tamu?><br>
                        <i><?= $rowTrans->nama_agen?></i><br>
                    </address>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <address>
                        <strong>Status Pembayaran:</strong><br>
                        <i><?= $rowTrans->proses_transaksi?></i><br>
                        <?= ($rowTrans->jenis_transaksi ==0 )? 'Pendaftaran Baru': ''?>
                        <?= ($rowTrans->jenis_transaksi ==1 )? 'Jasa Baru': ''?>
                        <?= ($rowTrans->jenis_transaksi ==2 )? 'Pelunasan': ''?>
                    </address>
                </div>
                <div class="col-sm-6 text-right">
                    <address>
                        <strong>Order Date:</strong><br>
                        <?= $rowTrans->id_transaksi?><br>
                        <?= date('d-m-Y',strtotime($rowTrans->tanggal))?><br>
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <td><strong>Item</strong></td>
                                <td class="text-center"><strong>Price</strong></td>
                                <td class="text-center"><strong>Quantity</strong></td>
                                <td class="text-right"><strong>Totals</strong></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $transaksi->data_seek(0);
                            $subtotal=0;
                            $tunggakan=0;
                            while($data = $transaksi->fetch_object()){
                                $subtotal +=  $data->total_transaksi;
                                $tunggakan +=  $data->sisa_pembayaran;

                                ?>
                                <tr>
                                    <td><?= $data->keterangan_transaksi?></td>
                                    <td class="text-center"><?= angkaIndo($data->total_transaksi)?></td>
                                    <td class="text-center">1</td>
                                    <td class="text-right"><?= angkaIndo($data->total_transaksi)?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td class="thick-line"></td>
                                <td class="thick-line"></td>
                                <td class="thick-line text-center"><strong>Total</strong></td>
                                <td class="thick-line text-right"><?= angkaIndo($subtotal)?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
