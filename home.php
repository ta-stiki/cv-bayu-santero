<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/17/2017
 * Time: 9:05 PM
 */
include_once __DIR__ . '/include/koneksi.php';
start_session();
$akses = checkAksesLogin();
if (!$akses){
    header('Location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">
    <title>CV. BAYU SANTERO</title>
    <!-- vendor css -->
    <?= loadCss([
        'lib/font-awesome/css/font-awesome.css',
        'lib/Ionicons/css/ionicons.css',
        'lib/perfect-scrollbar/css/perfect-scrollbar.css',
        'lib/jquery-toggles/toggles-full.css',
        'lib/rickshaw/rickshaw.min.css',
        'css/amanda.css',
    ])?>
    <?= loadJs([
        'lib/jquery/jquery.js',
        'lib/popper.js/popper.js',
        'lib/bootstrap/bootstrap.js',
    ])?>
    <style>
        .am-mainpanel{
            margin-top: auto;
            background-color: #D1D1D1;
        }
        .ui-datepicker .ui-datepicker-calendar th {
            padding: 0;
            text-align: center;
        }
        .bg-menu{
            background-color: #232e40;
            border-style: none solid none solid;
            border-color: #1A70BE;;
            border-width: 3px;
            /*border-left: 5px solid #1A70BE;*/
            /*border-right: 5px solid #1A70BE;*/
            color: white;
        }
    </style>
</head>
<body class="collapse-menu">
<div class="am-mainpanel">

    <div class="am-pagebody">
        <div class="row pd-b-20">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-body bd bd-1">
                        <div class="text-center">
                            <img src="<?= url('assets/img/logo.png')?>" alt="" class="image-responsive">
                            <p class="">CV BAYU SANTERO</p>
                        </div>
                    </div><!-- card-body -->
                </div>
            </div>
            <div class="col-lg-9 text-center">
                <img src="<?= url('assets/img/logo-big.jpeg')?>">
                <h3  class="tx-dark text-center">Selamat datang <?= $_SESSION['accApp']['nama_user']?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 pd-b-40">
                <div class="card">
                    <div class="card-body">
                        <p class="mg-b-0">
                            CV. BAYU SANTERO<br>
                            JL. Raya Semer NO 26<br>
                            Kerobokan<br>
                            Kuta-Badung<br>
                        </p>
                        <p class="mg-b-0">
                            ONLINE Services<br>
                            Call Hotline:<br>
                            +62 361736342<br>
                            +62 8123628737<br>
                            +62 81338656330<br>
                            E-Mail:<br>
                            poyakz@yahoo.co.id<br>
                        </p>
                        <br>
                        <div class="justify-content-center" id="datepicker"></div>

                    </div><!-- card-body -->
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row row-sm">
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=data-tamu')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Data Tamu</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=data-agen')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-address-book-o"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Data Agen</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=data-akun')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Data Akun</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=data-proses')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="icon ion-shuffle"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Data Proses</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=transaksi')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-newspaper-o"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Transaksi Masuk/Keluar</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=laporan-jurnal-umum')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-book"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Laporan Jurnal Umum</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=laporan-hutang-piutang')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-tags"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Laporan Hutang/Piutang</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=laporan-laba-rugi')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-file-text"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Laporan Laba Rugi</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=laporan-data-agen')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center">
                                    <i class="fa fa-file-text pd-t-20"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Laporan Data Agen</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=laporan-data-tamu')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-file-text"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Laporan Data Tamu</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=laporan-data-akun')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-file-text"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Laporan Data Akun</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('index.php?page=grafik-keuangan')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-bar-chart"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Grafik Keuangan</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 pd-b-40">
                        <a href="<?= url('logout.php')?>">
                            <div class="card bg-menu tx-white bd-0 wd-xs-300 ">
                                <div class="tx-32 tx-center pd-t-20">
                                    <i class="fa fa-sign-out"></i>
                                </div>
                                <div class="card-body bd-t-0 tx-center ">
                                    <h6 class="mg-b-3 ">Logout</h6>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

            </div>
        </div>

    </div><!-- am-pagebody -->
    <?php include_once __DIR__ .'/include/part/_footer.php'?>
</div><!-- am-mainpanel -->
<!--<script src="http://maps.google.com/maps/api/js?key=AIzaSyAEt_DBLTknLexNbTVwbXyq2HSf2UbRBU8"></script>-->
<?= loadJs([
    'lib/jquery-ui/jquery-ui.js',
    'lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
    'lib/jquery-toggles/toggles.min.js',
    'lib/d3/d3.js',
//    'lib/rickshaw/rickshaw.min.js',
    'lib/gmaps/gmaps.js',
    'lib/Flot/jquery.flot.js',
    'lib/Flot/jquery.flot.pie.js',
    'lib/Flot/jquery.flot.resize.js',
    'lib/flot-spline/jquery.flot.spline.js',
    'js/amanda.js',
    'js/ResizeSensor.js',
//    'js/dashboard.js',
])?>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            dayNamesMin: [ "Sn", "Sl", "Ra", "Ka", "Ju", "Sa", "Mi" ],
            monthNames: [ "Januari", "Februar1", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember" ]
        });
    } );
</script>
</body>
</html>

