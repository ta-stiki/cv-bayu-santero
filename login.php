<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/25/2017
 * Time: 9:23 AM
 */

include_once __DIR__ . '/include/koneksi.php';
start_session();

$username = '';
$password = '';
$login = '';
if($_POST){
    $username = escape($_POST['username']);
    $password = escape($_POST['password']);
    $login = doLogin($username,$password);
}

$akses = checkAksesLogin();
if ($akses){
    header('Location:home.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">


    <title>Login Aplikasi</title>
    <?= loadCss([
        'lib/font-awesome/css/font-awesome.css',
        'lib/Ionicons/css/ionicons.css',
        'lib/perfect-scrollbar/css/perfect-scrollbar.css',
        'lib/jquery-toggles/toggles-full.css',
        'lib/rickshaw/rickshaw.min.css',
        'css/amanda.css',
    ])?>
    <?= loadJs([
        'lib/jquery/jquery.js',
        'lib/popper.js/popper.js',
        'lib/bootstrap/bootstrap.js',
        'lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
        'js/amanda.js',
    ])?>
</head>

<body>

<div class="am-signin-wrapper">
    <div class="am-signin-box">
        <div class="row no-gutters">
            <div class="col-lg-5">
                <div>
                    <h2>CV BAYU SANTERO</h2>
                    <p>Selamat Datang di Aplikasi Keuangan CV BAYU SANTERO</p>
                    <p>Silahkan login terlebih dahulu sebelum masuk kedalam aplikasi.</p>
                    <hr>
                    <p><?= $login?></p>
                </div>
            </div>
            <div class="col-lg-7">
                <h5 class="tx-gray-800 mg-b-25">Login User</h5>
                <form action="" method="post">
                    <div class="form-group">
                        <label class="form-control-label">Username:</label>
                        <input value="<?= $username?>" type="text" name="username" class="form-control" placeholder="Masukkan Username">
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="form-control-label">Password:</label>
                        <input value="<?= $password?>" type="password" name="password" class="form-control" placeholder="Masukkan Password">
                    </div><!-- form-group -->
                    <button type="submit" class="btn btn-block">Login</button>
                </form>
            </div><!-- col-7 -->
        </div><!-- row -->
    </div><!-- signin-box -->
</div><!-- am-signin-wrapper -->
</body>
</html>

