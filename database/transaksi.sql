/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50624
 Source Host           : 127.0.0.1:3306
 Source Schema         : acc_sriwijayanti

 Target Server Type    : MySQL
 Target Server Version : 50624
 File Encoding         : 65001

 Date: 01/02/2018 16:22:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi`  (
  `id_transaksi` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_tamu` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan_transaksi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `total_transaksi` decimal(15, 2) DEFAULT NULL,
  `sisa_pembayaran` decimal(15, 2) DEFAULT NULL,
  `proses` int(2) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `induk` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jenis_transaksi` int(2) DEFAULT NULL,
  `status` enum('Lunas','Belum Lunas') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tipe_transaksi` enum('Masuk','Keluar','') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `no_bukti` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`) USING BTREE,
  INDEX `fk_transaksi_tamu_1`(`id_tamu`) USING BTREE,
  INDEX `fk_transaksi_tb_user_1`(`id_user`) USING BTREE,
  CONSTRAINT `fk_transaksi_tb_user_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
