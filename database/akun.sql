/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50624
 Source Host           : 127.0.0.1:3306
 Source Schema         : acc_sriwijayanti

 Target Server Type    : MySQL
 Target Server Version : 50624
 File Encoding         : 65001

 Date: 01/02/2018 16:21:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for akun
-- ----------------------------
DROP TABLE IF EXISTS `akun`;
CREATE TABLE `akun`  (
  `kode_rekening` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `nama_rekening` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `klasifikasi` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `debit` decimal(15, 2) DEFAULT NULL,
  `kredit` decimal(15, 2) DEFAULT NULL,
  PRIMARY KEY (`kode_rekening`) USING BTREE,
  INDEX `fk_akun_akun_klasifikasi_1`(`klasifikasi`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of akun
-- ----------------------------
INSERT INTO `akun` VALUES ('1.1', 'Kas', 'A', 30000.00, 10000.00);
INSERT INTO `akun` VALUES ('1.2', 'Piutang Usaha', 'A', 15000.00, 0.00);
INSERT INTO `akun` VALUES ('1.3', 'Perlengkapan Kantor', 'A', NULL, NULL);
INSERT INTO `akun` VALUES ('1.5', 'Peralatan Kantor', 'A', NULL, NULL);
INSERT INTO `akun` VALUES ('2.1', 'Hutang', 'B', 0.00, 90000.00);
INSERT INTO `akun` VALUES ('3.1', 'Modal', 'C', NULL, NULL);
INSERT INTO `akun` VALUES ('4.1', 'Pendapatan Usaha', 'D', 0.00, 45000.00);
INSERT INTO `akun` VALUES ('5.1', 'Beban Gaji', 'F', NULL, NULL);
INSERT INTO `akun` VALUES ('5.2', 'Beban Iklan', 'F', NULL, NULL);
INSERT INTO `akun` VALUES ('5.3', 'Beban Sewa Kantor', 'F', 100000.00, 0.00);
INSERT INTO `akun` VALUES ('5.4', 'Beban Rupa-rupa', 'F', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
