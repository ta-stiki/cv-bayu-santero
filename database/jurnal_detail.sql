/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50624
 Source Host           : 127.0.0.1:3306
 Source Schema         : acc_sriwijayanti

 Target Server Type    : MySQL
 Target Server Version : 50624
 File Encoding         : 65001

 Date: 01/02/2018 16:22:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for jurnal_detail
-- ----------------------------
DROP TABLE IF EXISTS `jurnal_detail`;
CREATE TABLE `jurnal_detail`  (
  `id_jurnal` int(11) DEFAULT NULL,
  `kode_akun` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `debit` decimal(15, 2) DEFAULT NULL,
  `credit` decimal(15, 2) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posting` int(1) DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_jurnal_detail_jurnal_1`(`id_jurnal`) USING BTREE,
  INDEX `fk_jurnal_detail_akun_1`(`kode_akun`) USING BTREE,
  CONSTRAINT `fk_jurnal_detail_jurnal_1` FOREIGN KEY (`id_jurnal`) REFERENCES `jurnal` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
