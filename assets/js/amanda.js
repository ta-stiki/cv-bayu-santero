/*!
 * Amanda Responsive Bootstrap 4 Admin Template
 * Version: 2.0.0
 * Author: ThemePixels (@themepixels)
 * URL: http://themepixels.me/amanda
 *
**/

'use strict';

$(document).ready(function(){

  $('.numberinput').inputmask("numeric", {
      radixPoint: ".",
      groupSeparator: ",",
      digits: 2,
      removeMaskOnSubmit:true,
      autoGroup: true,
      // prefix:, //No Space, this will truncate the first character
      rightAlign: true,
      oncleared: function () {
          $(self).val('');
      }
  });
  // Make the left sidebar scrollable
  $('.am-sideleft .tab-pane').perfectScrollbar();

  // Left Sidebar Tab
  $('.am-sideleft-tab .nav-link').on('click', function(e){
    e.preventDefault();

    // finding and unsetting current active tab
    $('.am-sideleft-tab .nav-link').each(function(){
      $(this).removeClass('active');
    });

    // make this as a new active tab
    $(this).addClass('active');

    // finding and hiding current tab pane
    $('.am-sideleft .tab-pane').each(function(){
      $(this).removeClass('active');
    });

    // making this target as a new active pane
    $($(this).attr('href')).addClass('active');
  });

  // show sub menu in left sidebar
  $('.show-sub + .nav-sub').slideDown();

  // Toggles a class that will show/hide left menu
  $('#naviconLeft').on('click', function(e) {
    e.preventDefault();
    $('body').toggleClass('collapse-menu');
  });

  // Toggles a class that will shows/left menu
  // and push the mainpanel (mobile only)
  $('#naviconLeftMobile').on('click', function(e) {
    e.preventDefault();
    $('body').toggleClass('show-left');
  });

  // In mobile it shows search input form when clicking search button
  // This prevent search form submission when input value is empty
  $('#searchBtn').on('click', function(e) {
    var parent = $(this).parent();
    parent.toggleClass('show-search');
    if(parent.find('input').val() === '') {
      e.preventDefault();
    }
  });

  // When in mobile, this will hide input form when value is empty
  $('#searchBar input').on('focusout', function() {
    if($(this).val() === '') {
      $(this).closest('form').removeClass('show-search');
    }
  });


  // Show/hide sub navigation of sidebar menu
  $('.with-sub').on('click', function(e){
    e.preventDefault();
    $(this).next().slideToggle();
    $(this).toggleClass('show-sub');
  });

  // Toggles
  $('.toggle').toggles({
    on: true,
    height: 22
  });

  // highlight syntax highlighter
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });

});

function groupTable($rows, startIndex, total){
	if (total === 0){
		return;
	}
	var i , currentIndex = startIndex, count=1, lst=[];
	var tds = $rows.find('td:eq('+ currentIndex +')');
	var ctrl = $(tds[0]);
	lst.push($rows[0]);
	for (i=1;i<=tds.length;i++){
		if (ctrl.text() ==  $(tds[i]).text()){
			count++;
			$(tds[i]).addClass('deleted');
			lst.push($rows[i]);
		}else{
			if (count>1){
				ctrl.attr('rowspan',count);
				groupTable($(lst),startIndex+1,total-1)
			}
			count=1;
			lst = [];
			ctrl=$(tds[i]);
			lst.push($rows[i]);
		}
	}
}