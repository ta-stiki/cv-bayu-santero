<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/24/2017
 * Time: 10:38 PM
 */

ini_set('display_errors', 0);
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);

$page = isset($_GET['page'])? $_GET['page']:null;
$action = isset($_GET['action'])? $_GET['action']:null;


switch ($page){
    case 'data-tamu':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'user-profile':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-tamu-add':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-tamu-update':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-tamu-history':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-agen':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-agen-add':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-agen-update':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-kewarganegaraan':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-kewarganegaraan-add':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-kewarganegaraan-update':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-akun':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-akun-add':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-akun-update':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-proses':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-proses-add':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'data-proses-update':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'transaksi':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'transaksi-masuk':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'transaksi-masuk-add':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'transaksi-masuk-update':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;

    case 'transaksi-keluar':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;

    case 'transaksi-keluar-add':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'transaksi-keluar-update':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'posting':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;


    case 'laporan-jurnal-umum':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'laporan-hutang-piutang':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'laporan-laba-rugi':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'laporan-data-agen':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'laporan-data-tamu':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'laporan-data-akun':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'grafik-keuangan':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    case 'grafik-keuangan-print':
        include_once __DIR__ . "/include/page/_{$page}.php";
        break;
    default:
        break;

}


?>